#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense> 
#include <vector>
//#include <chrono>
#include <ctime>
#include "MatrixFree.h"


void cg(const MatrixAction& matrix_action, const Eigen::VectorXd& x)
{
  // Solve 
}

int main()
{
	// Let's try to implement iterative CG method
	int n = 4;

	// Matrix
	// well of course you need to check
	// real, symmetry, positive-definite
	// but I make this matrix so yes it is
	MassMatrixOperator M(4);
	//std::cout << mat;

	// Vector
	Eigen::VectorXd b(n); 
	b << 1.0, 2.0, 3.0, 4.0;

	// initialise
	Eigen::VectorXd x(n);
	Eigen::VectorXd r(n), rnew(n);
	Eigen::VectorXd p(n);
	int iter = 0;
	double tol = 1e-6; 
	double alpha, beta;

	x = Eigen::VectorXd::Zero(n);
	r = b - mat.matvecmult(x);
	p = r;
	double err = sqrt(r.dot(r));

	while (true)
	{	
		alpha = (r.dot(r))/(p.dot(M.action(p))));
		x = x + alpha*p;
		rnew = r - alpha*M.action(p);
		err = sqrt(rnew.dot(rnew));
		if (err < tol) 
		{
			break; 
		}
		beta = (rnew.dot(rnew)) / (r.dot(r)); 
		p = rnew + beta*p;
		r = rnew;
		iter++;
	}
	std::cout << "Solution using matrix-free method: \n";
	std::cout << x << std::endl;
	
	// ------------- CHECKING ----------//
	// Let's try and see the errors compare to direct solver
	Eigen::MatrixXd A(4,4);
	A << (1.0/3.0), (1.0/6.0), 0.0, 0.0,
		 (1.0/6.0), (2.0/3.0), (1.0/6.0), 0.0,
		 0.0, (1.0/6.0), (2.0/3.0), (1.0/6.0),
		 0.0, 0.0, (1.0/6.0), (1.0/3.0);
	Eigen::VectorXd btest(4); 
	btest << 1.0, 2.0, 3.0, 4.0;

	// Normal equation
	std::cout << "The solution using normal equations is:\n"
	<< (A.transpose() * A).ldlt().solve(A.transpose() * btest) << std::endl;
 
  	// Jacobi
 	std::cout << "The least-squares Jacobi solution is:\n"
	<< A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(btest) << std::endl;

  	// Column Pivoting QR decomposition
  	std::cout << "The solution using the QR decomposition is:\n"
    << A.colPivHouseholderQr().solve(btest) << std::endl;

	return 0;
}
