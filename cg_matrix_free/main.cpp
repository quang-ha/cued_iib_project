#include <iostream>
#include "Eigen/Core"
#include <vector>
//#include <chrono>
#include <ctime>
#include "MatrixFree.h"

using namespace std;
using namespace Eigen;

int main()
{
	int n = 4;
	VectorXd b(n); 
	b << 1, 2, 3, 4;

	MassMatrixOperator m(4);
	cout << m.element_mat() << endl;
	cout << m.mat() << endl;

	return 0;
}
