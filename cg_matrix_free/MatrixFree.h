// This header files include the class MatrixFree
// which contains method of free

// currently include Mass Matrix in 1D
#ifndef MATRIX_FREE
#include <Eigen/Core> // to include Eigen3
#define MATRIX_FREE
#include <iostream>

//------****MatrixAction***---------//
class MatrixAction
// To have both 
{
  // These are extras, the current functions will 
  // focus on using 1D + linear shape functions!!	
  protected:
    int _nodes;
    int _geometric_dimension;
    int _degree; 
    // Need to store the elemental matrix
    Eigen::MatrixXd _element_matrix;

    // The mass matrix
    // private members = underscore
    Eigen::MatrixXd _A;

 public:
  virtual ~MatrixAction() {}

  // Compute matrix action y = Ax
  // MATRIX-FREE!!
  // this return and update x, no y created
  virtual void action(Eigen::VectorXd& x);
};

//------****MassMatrixOperator***---------//
class MassMatrixOperator : public MatrixAction
{
  public:
    // CONSTRUCTORS
    // empty constructor, create 2x2 1D linear Mass Matrix
    //MatrixFree();
    // size only
    MassMatrixOperator(const int n);
    // size and dimension - NOT WORKING
	MassMatrixOperator(const int n,const int d);
	// size, dimension and degree - NOT WORKING
	MassMatrixOperator(const int n, const int d, const int deg);

	// return one value of the matrix
	double get_value(int x, int y);

	virtual void action(Eigen::VectorXd& x)
	{ 
	  //this will return the original matrix-vector multiplcation
	  // of y = _M*x
	  x *= _A; 	
	}

	const Eigen::MatrixXd& mat() const { return _A; }
	const Eigen::MatrixXd& element_mat() const { return _element_matrix; }

	// Output on ostream
	//friend std::ostream& operator << (std::ostream &out, MatrixFree mat);

	// Generate elemental matrix
	virtual void gen_element_matrix();

	// DESTRUCTOR
	// Do we really need this?
	virtual ~MassMatrixOperator();
};

//------****StiffMatrixOperator***---------//
class StiffMatrixOperator : public MatrixAction
{
  public:
    // CONSTRUCTORS
    // empty constructor, create 2x2 1D linear Stiff Matrix
    // size only
    StiffMatrixOperator(const int s);
    // size and dimension - NOT WORKING
	StiffMatrixOperator(const int s,const int d);
	// size, dimension and degree - NOT WORKING
	StiffMatrixOperator(const int s, const int d, const int deg);

	// return one value of the matrix
	double get_value(int x, int y);

	virtual void action(Eigen::VectorXd& x)
	{ 
	  //this will return the original matrix-vector multiplcation
	  // of y = _K*x
	  x *= _A; 	
	}

	const Eigen::MatrixXd& mat() const { return _A; };
	const Eigen::MatrixXd& element_mat() const { return _element_matrix; }

	// Output on ostream
	//friend std::ostream& operator << (std::ostream &out, MatrixFree mat);

	// Generate elemental matrix
	virtual void gen_element_matrix(); 

	// DESTRUCTOR
	// Do we really need this?
	virtual ~StiffMatrixOperator();
};

#endif
