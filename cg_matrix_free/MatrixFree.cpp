#include "MatrixFree.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
//------*********MatrixAction******-----//
void MatrixAction::action(Eigen::VectorXd& x)
{
  // NEED TO USE _element_matrix VARIABLE
  // First element
  double a = (double)x[0], aNext;
  x[0] = (1.0/3.0)*x[0] + (1.0/6.0)*x[1];

  // Then iterate from the second element
  for (int j=1; j<_nodes; j++)
    {
      aNext = x[j];	
      x[j] = (1.0/6.0)*a + (2.0/3.0)*x[j] + (1.0/6.0)*x[j];
      a = aNext;
    }
}

//------**********MassMatrixOperator********----//
void MassMatrixOperator::gen_element_matrix()
{
	_element_matrix = Eigen::MatrixXd::Zero(2, 2);
}

// size only
MassMatrixOperator::MassMatrixOperator(const int n)
{
  _nodes = n; _geometric_dimension = 1; _degree = 1;
  // Generate the element matrix
  // NEED A FUNCTION TO CALCULATE THE DIMENSION OF THE ELEMENTAL MATRIX
  _element_matrix = Eigen::MatrixXd(_geometric_dimension+1, _geometric_dimension+1);
  _element_matrix << (1.0/3.0), (1.0/6.0),
                     (1.0/6.0), (1.0/3.0); 


  // Throw items into the global matrix
  _A = Eigen::MatrixXd::Zero(_nodes, _nodes);
  for (int i=0; i<_nodes-1; i++)
  {
    for (int k=0; k<2; k++)
  	  {
  	    for (int m=0; m<2; m++)
  	      {
  	        _A(i+k,i+m) += _element_matrix(k,m);
  	      }
  	  }
  }
}

// size and dimension - NOT WORKING
MassMatrixOperator::MassMatrixOperator(const int n, const int d)
{
  _nodes = n; _geometric_dimension = d; _degree = 1;
}

// size, dimension and degree - NOT WORKING
MassMatrixOperator::MassMatrixOperator(const int n, const int d, const int deg)
{
  _nodes = n; _geometric_dimension = d; _degree = deg;
}

// Destructor
MassMatrixOperator::~MassMatrixOperator()
{
}

// Elemental Mass matrix for 1D linear element
// Me = [1/3 1/6
//		[1/6 1/3]

// return a value of the matrix
double MassMatrixOperator::get_value(int x, int y)
{
	double val;
	// check the size of matrix
	if ((x < _nodes) && (0 <= x) && (y < _nodes) && (y>=0))
	{
		// now let's choke out some numbers
		switch (x-y)
		{
			case -1:
				val = 1.0/6.0;
				break;
			case 1: 
				val = 1.0/6.0;
				break;
			case 0:
				if ((y==0) or (y==_nodes-1))
				{
					val = 1.0/3.0;
				}
				else
				{
					val = 2.0/3.0;
				}
				break;
			default: 
				val = 0.0;
				break;
		}
	}
	else
	{
		perror("Requested index exceed matrix dimension. \n"); 
		exit(EXIT_FAILURE);
	}
	return val;
}

// friend std::ostream& MassMatrixOperator::operator<<(std::ostream &out, MatrixFree mat);
//   {
//   	for (int i=0; i<mat.size; i++)
//   	{
//   		for (int j=0; j<mat.size; j++)
//   		{
//   			out << mat.get_value(i, j) << " ";
//   		}
//   		out << "\n";
//   	}
//   	return out;
//   }
//---------*****END OF MassMatrixOperator****---//