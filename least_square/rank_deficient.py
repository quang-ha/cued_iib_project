import matplotlib.pyplot as plt
import numpy as np

# Get the vandermoore matrix
# x = np.array([1,2,3,4,5,6,7,8,9,10])
num_point = 20
x = np.linspace(0.1,0.5,num_point)
# Degree of polynomial
num_deg = 15
A = np.vander(x,num_deg)

# Make A rank deficient
for i in range(int(num_deg*0.4), num_deg):
    A[:,i] = A[:,i - int(num_deg*0.4)] - A[:,i - int(num_deg*0.4) + 1]

U, s, V = np.linalg.svd(A, full_matrices=False)
print s.shape
print np.linalg.matrix_rank(A)

# Try some noise
x_n = x + 0.1*np.random.normal(0,1,num_point)
A_n = np.vander(x_n,num_deg)
U_n, s_n, V_n = np.linalg.svd(A_n, full_matrices=False)
print np.linalg.matrix_rank(A_n)

# Plot it
fig = plt.figure()
ax = plt.gca()
ax.plot(s, 'o-', c='blue', label='Rank deficient')
ax.set_yscale('log')
ax.set_ylabel('Singular values')
fig.savefig("rank_def.png", dpi=400, transparent=True)

# Plot it
fig = plt.figure()
ax = plt.gca()
ax.plot(s_n, 'o-', c='blue', label='Ill-conditioning')
ax.set_yscale('log')
ax.set_ylabel('Singular values')
# ax.plot(s_n, 'o-', c='red', label='Ill-conditioning')
# plt.legend()
# plt.show()
fig.savefig("ill_cond.png", dpi=400, transparent=True)
