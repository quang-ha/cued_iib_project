from dolfin import *
import scipy.sparse.linalg
import scipy.sparse
import numpy as np

import matplotlib.pyplot as plt

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

#### Let's try and capture the sparse matrix into Scipy
parameters['linear_algebra_backend'] = 'Eigen'

# Create mesh and define function space
num_point = 100
mesh = IntervalMesh(num_point, 1.0, 5.0)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
# plot(u_f, interactive=True)
a = inner(grad(u), grad(v))*dx

# Get the sparse matrix from a
K = assemble(a)
G = as_backend_type(K).sparray().tocsc()

# Now take the observation
u_0 = Expression("2.0*exp(-(pow(x[0] - 2.0, 2)) / 1.5) + 3.0*exp(-(pow(x[0] - 4.0, 2)) / 1.0)")
u_f = Function(V)
u_f.interpolate(u_0)
u_array = u_f.vector()[:]
f_1 = G.dot(u_array)

# Creating weighting matrix
identity_reg = scipy.sparse.identity(num_point+1)

# Exact solution of f
# f_exact = Expression("-(8.0/3.0)*exp(-(pow(x[0] - 2.0, 2)) / 1.5) - 6.0*exp(-(pow(x[0] - 4.0, 2)) / 1.0)")
# Now try some random function
# f_unexact = Expression("-(8.0/3.0)*exp(-(pow(x[0] - 1.2, 2)) / 1.5) - 6.0*exp(-(pow(x[0] - 4.2, 2)) / 1.0)")
# f_f = Function(V)
# f_f.interpolate(f_unexact)
# f_0 = f_f.vector()[:]
f_0 = f_1[::-1]

fig = plt.figure(figsize=(10,8))
plt.plot(mesh.coordinates(), u_array, linewidth=2.0, label='Regularised')

# Exact solution, with no weighting toward regularised
Q = G.T.dot(G) #+ eta*identity_reg
b = G.dot(f_0) #+ eta*identity_reg.dot(u_array)
# And now let's solve for the inverse solution
[u_result, tol] = scipy.sparse.linalg.cg(Q, b, tol=1e-8)
plt.plot(mesh.coordinates(), u_result, linewidth=2.0, label='Exact solution')

# Now add regularised term
for eta in [0.01, 0.05]:
    Q = G.T.dot(G) + eta*identity_reg
    b = G.dot(f_0) + eta*identity_reg.dot(u_array)
    # And now let's solve for the inverse solution
    [u_result, tol] = scipy.sparse.linalg.cg(Q, b, tol=1e-8)
    plt.plot(mesh.coordinates(), u_result, linewidth=2.0, label=r'$\eta$ = {:1.2f}'.format(10*eta))

plt.legend(loc=3)
plt.grid()
plt.xlabel('Mesh point')
plt.ylabel(r'$x$')
# plt.show()
fig.savefig("weighting_ls.png", dpi=400, transparent=True)


# Save solution in VTK format
# file = File("poisson.pvd")
# file << u

# Plot solution
# plot(u, interactive=True)
