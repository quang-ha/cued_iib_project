% QR-algorithm

clear all
close all

% matrix with eigenvalues 3,2 and 1
B=[ 1  0  1;
    2  3 -1;
   -2 -2  2]


for i=1:25
    pause
    disp(['iteration ' num2str(i) ': performing [Q,R]=qr(B) and B=R*Q'])
    [Q,R]=qr(B);
    B=R*Q  
end

disp('not the right ordering is achieved - lets perform another 45 steps')

for i=26:70
    pause
    disp(['iteration ' num2str(i) ': performing [Q,R]=qr(B) and B=R*Q'])
    [Q,R]=qr(B);
    B=R*Q  
end

disp('now the diagonal elements are ordered by magnitude')