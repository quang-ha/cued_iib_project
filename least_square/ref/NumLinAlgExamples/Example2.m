% Power-method without normation step

clear all
close all

% another nice matrix from the lecture, with maximal eigenvalue 0.8928
B=[0.2  0.3  0.4;
   0.6 -0.1  0.5;
   0.2  0.5  0.1]

% random starting vector
x0=randn(3,1)

x=x0;

for i=1:10
    pause
    disp(['iteration ' num2str(i) ': performing B*x'])
    x=B*x
    
end