% Power-method for difficult cases

close all
clear all

randn('seed',3)

X1=randn(3,3);
[X,R]=qr(X1);
D=diag([10,9,8]);

% matrix with the eigenvalues 10,9 and 8 (not very nice separated)
A=X'*D*X

% random starting vector
x0=randn(3,1)

[eigenvalue,eigenvector] = PowerMethod(A,x0,25)

pause

disp('make starting vector x0 orthogonal to eigenvector')

% special starting vector with no component into u1-direction
x1=X(:,1);
x0orth=x0-(x0'*x1)/(x1'*x1)*x1

[eigenvalue,eigenvector] = PowerMethod(A,x0orth,41)


