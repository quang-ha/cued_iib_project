% Inverse Iteration

clear all
close all

% matrix from the lecture, with smallest eigenvalue 0.2
B=[0.2  0.3  0.4;
   0.6 -0.1  0.5;
   0.2  0.5  0.1]

% random starting vector
x0=randn(3,1)

[eigenvalue,eigenvector]=InverseIteration(B,x0,10)