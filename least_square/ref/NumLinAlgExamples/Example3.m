% Power-method

clear all
close all

% matrix from Example2, with maximal eigenvalue 0.8928
B=[0.2  0.3  0.4;
   0.6 -0.1  0.5;
   0.2  0.5  0.1]

% random starting vector
x0=randn(3,1)

[eigenvalue,eigenvector]=PowerMethod(B,x0,10)

