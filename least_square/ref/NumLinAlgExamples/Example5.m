% Nonconvergence of Power-method

close all
clear all

randn('seed',4)

X1=randn(3,3);

% special matrix with two dominant eigenvalues of magnitude 1
A=inv(X1)*diag([1 -1 0.2])*X1

% random starting vector
x0=randn(3,1)

% no convergence of Power-method
[eigenvalue,eigenvector] = PowerMethod(A,x0,20)
