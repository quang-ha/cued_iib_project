function [eigenvalue,eigenvector] = InverseIteration(A,x0,iter)

x=x0;

for i=1:iter
    pause
    disp(['iteration ' num2str(i) ': performing  A*y=x for y'])
    y=A\x; % is the same as y=A^(-1)*x
    k=norm(y)
    x=y/k
end

eigenvalue=1/k;
eigenvector=x;