function [eigenvalue,eigenvector] = PowerMethod(A,x0,iter)

x=x0;

for i=1:iter
    pause
    disp(['iteration ' num2str(i) ': performing  A*x'])
    x=A*x;
    k=norm(x)
    x=x/k
end

eigenvalue=k;
eigenvector=x;