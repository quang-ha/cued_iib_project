% powers of a matrix

clear all
close all

% nice stochastic matrix from the lecture, with maximal eigenvalue 1
A=[0.2 0.3 0.4;
   0.6 0.2 0.5;
   0.2 0.5 0.1]

% random starting vector
x0=randn(3,1)

x=x0;

for i=1:10
    pause
    disp(['iteration ' num2str(i) ': performing A*x'])
    x=A*x
    
end

