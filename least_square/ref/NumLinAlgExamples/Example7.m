% Power-method with Deflation

clear all
close all

X1=randn(3,3);

% special matrix with the eigenvalues 3,2 and 1
A=inv(X1)*diag([3 2 1])*X1

% random starting vector
x0=randn(3,1)

% Power-method for left eigenvector
[eigenvalue1,eigenvector1] = PowerMethod(A,x0,15)

% DEFLATION 1

% eigenvalue-w'*eigenvector=0
w1=[1;0;0]*eigenvalue1/eigenvector1(1)

B=A-eigenvector1*w1'

% random starting vector
x0=randn(3,1);

[eigenvalue2,eigenvector2] = PowerMethod(B,x0,15)

% DEFLATION 2

% eigenvalue-w'*eigenvector=0
w2=[1;0;0]*eigenvalue2/eigenvector2(1)

C=B-eigenvector2*w2'

% random starting vector
x0=randn(3,1);

[eigenvalue3,eigenvector3] = PowerMethod(C,x0,15)



