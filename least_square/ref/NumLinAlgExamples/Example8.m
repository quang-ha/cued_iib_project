% QR-algorithm

clear all
close all

% matrix with eigenvalues 3,2 and 1
A=[ 1 -1 -1;
    4  6  3;
   -4 -4 -1]


for i=1:10
    pause
    disp(['iteration ' num2str(i) ': performing [Q,R]=qr(A) and A=R*Q'])
    [Q,R]=qr(A);
    A=R*Q  
end



