
options = odeset('OutputFcn',@odephas2)

axis([-6 6  -6 6])
hold on
 
for i=1:15
[t,y]=ODE45(@schwinger,[0 20], [6,-6+0.4*i]', options);
[t,y]=ODE45(@schwinger,[0 20], [-6,0+0.4*i]', options);
[t,y]= ODE45(@schwinger,[0 20], [0,-2+4/15*i]', options);
end