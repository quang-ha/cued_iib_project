%2Anwendung der Wellengleichungsprogramme

%y0= zeros(78,1);
x=[1:39]'/40;
y0(1:39)= sin(pi*x); % Anfangswerte f�r die Funktion
y0(40:78)= 0.5- abs(x-1); % Anfangswerte f�r die Ableitung

[t,y]= ode45(@(t,y)schwingung(t,y,@huepfer,@dirichlet0, @dirichlet0),[0,3], y0 );

mesh(t,x,y(:,1:39)');



