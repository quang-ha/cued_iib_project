function[erg] = schwingung(t,y,anreg,randl,randr)
% Linienmethode f�r die Schwingungsgleichung  
% u_tt = c*u_xx + anreg(t,u);
% u(0,t) = randl(t); u(1,t) = randr(t);
%
% Anfangsbedingungen werden direkt als Anfangswerte in 
% den ODE-Integrator eingegeben 
% y(1)....y(n) enth�lt die Seilpositionen
% y(n+1)  y(2n) enth�lt die Geschwindigkeiten
m=length(y);
n=m/2;
erg=zeros(m,1);

h=1/(n+1);
edh2=1/h^2;
erg(1:n) = y(n+1:2*n);
erg(n+1:2*n)= -2*y(1:n);
erg(n+1:2*n-1)=erg(n+1:2*n-1)+ y(2:n);
erg(n+2:2*n)= erg(n+2:2*n)+y(1:n-1);
erg(n+1)=erg(n+1)+feval(randl,t);
erg(2*n)=erg(2*n)+feval(randr,t);
erg(n+1:2*n)=erg(n+1:2*n)*edh2;
erg(n+1:2*n) = erg(n+1:2*n)+ feval(anreg,t,y);


