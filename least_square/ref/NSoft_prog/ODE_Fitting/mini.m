function res = mini(z,tm,ym)
a=z(1); b=z(2); c=z(3); s=z(4);
[t,y]=ode45(@(t,y)coeff(t,y,z(1:3)),tm,s);
res=norm(y-ym)^2;

