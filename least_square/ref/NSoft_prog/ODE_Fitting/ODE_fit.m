%Main programm to fit ODE-Solution to data
% Assume that the data in the vectors tm and ym have been measured
tm =[1 2 2.1 3 3.1 4 4.2 5]';
ym =[  0.9,1.2,1.3,1.8,1.8,2.5,2.7, 3.2]';
plot(tm,ym,'or');
hold on
%Assume given a model ode : y' = a*t+b*y+c*y^2; y(1)=s
%Task: Determine a,b,c,s in such a way, that
%  sum_of_squares= (y(tm(1))-ym(1))^2+ (y(tm(2))-ym(2))^2+....+(y(tm(8))-ym(8))^2
% is minimal (sum_of_squares is compute in suboutine "mini").
%
% Apply a 2-phase-algorithm
%
%Phase 1
%   write down ode for every point tm(1),...,tm(8)
%    y'(tm(1)) = tm(1)*a + y(tm(1))*b + y(tm(1))^2*c
%    y'(tm(2)) = tm(2)*a + y(tm(2))*b + y(tm(2))^2*c
%      .          .           .           .
%      .          .           .           .
%    y'(tm(8)) = tm(8)*a + y(tm(8))*b + y(tm(8))^2*c
%
%  If (y'(tm(1)),...,y'(tm(8)))= transpose(yp)  where known, this would be
%  an overdeterminedsystem of linear equations  Ax = yp  (with x=(a,b,c)')
%  which could be solved  by least squares  x = A\yp.
%
% We compute approximate derivatives by differentiating the interpolating
% spline (if the errors in the measurements ym are large, one would use
% a "smoothing spline" instead.
yp = (spline(tm,ym,tm+1e-8)-ym)*1e8;
% Set up the marix of h above linear system
A = [tm, ym, ym.^2];
% Find approximate values (a,b,c)=x
x=A\yp
%
% Integrate ode with these values an ym(1) as approximate start value
[t,y] = ode45(@(t,y)coeff(t,y,x),[tm(1),tm(8)],ym(1));
plot(t,y)
pause
%
% Now minimize sum of squares with respect to parameters and starting value
% using the approximations as starting values 
start=[x; ym(1)];
zr= fminsearch(@(z)mini(z,tm,ym),start')
%zr wants to say "the right z"
% Plot final approximation
[tout,yout]=ode45(@(t,y)coeff(t,y,zr(1:3)),[tm(1),tm(8)],zr(4));
plot(tout,yout,'r')