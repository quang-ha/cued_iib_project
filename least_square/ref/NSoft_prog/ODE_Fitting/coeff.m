function res = coeff(t,y,zr)
a=zr(1); b=zr(2);c=zr(3);
res= t*a+b*y+c*y^2;
