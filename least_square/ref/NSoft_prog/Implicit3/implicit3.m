 function res = implicit3(t,y,fun)
% Assumption: Let fun in C^2(R^n,R^(n-2)) and Rank (Jacobian(fun)) = n-2
% in the (say convex and open ) y-region  R we are interested in.
% If there is a y* in R with f(y*)=0, the equation fun(y)= 0 defines 
% a 2D-surface in R through y*.
%
% The programme defines a vectorfield (to be used as right-hand side of an 
% ordinary differential equation) such that the integration of this diffe- 
% rential equation defines a curve,which leads "right back to the surface",
% which means that it takes - in some  measure - the shortest path to the 
% solution manifold.
% EXAMPLE
%        function res = Ellipsoid(y)
%        % defines the ellipsoid in R^3
%        res = y(1)^2+(2*y(2))^2+(3*y(3))^2-14;
%
%        T=10; y0=[  1.0798    1.3597    2.1960]'; 
%        [t,y] = ode45(@implicit3(t,y,@Ellipsoid),[0,T],y0)
%        % approaches the Ellipsoid "orthogonally" towards y*=[1,1,1]'.
%
[J,F] = Jakob(fun,y);
n=null(J);
JJ=[J;n'];
res = -JJ\[F;[0,0]'];



