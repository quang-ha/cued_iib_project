function res = Ellipsoid(y)
%        % defines the ellipsoid in R^3
        res = y(1)^2+(2*y(2))^2+(3*y(3))^2-14;