function erg = kreis_gerade(z)
x=z(1); y=z(2);
erg(1) = x^2+y^2-1;
erg(2)=x+y;
erg=erg';