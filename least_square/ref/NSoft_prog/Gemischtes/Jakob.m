function[erg1,erg2] = Jakob(fun,x)
% Approximiert die Jakobimatrix der auf fun �bergebenen 
% Funktion an der Stelle x
re
n=length(x);                            % Bestimme dim von x
ee = sqrt(eps);                         % Bestimme St�rungsgr��e
basis_wert = fun(x);                    % Bestimme Funktionswert
erg2=basis_wert;                        % R�ckgabe Funktionswert
m=length(basis_wert);                   % Dimnsion Bildraum
for k=1:n                               % Schleife �ber Urbilddimensionen
    vari = max(1,abs(x(k)))*ee;         % Bestimme Variation von x(k)
        save =x(k);                     % Sichere x(k) 
        x(k) = x(k)+vari;               % St�re x(k)
    wert=fun(x);                        % Werte fun an gest�rten x aus
    erg1(1:m,k)= (wert-basis_wert)/vari;% Approximiere k-te Spalte von J 
    x(k)=save;                          % Stelle x(k) wieder her
end
