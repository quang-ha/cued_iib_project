function erg= vlp(t,y,a,b,g,d)
%Voltera-Lotka- oder R�ube-Beute-Modell
erg(1) = a*y(1)-b*y(2)*y(1);
erg(2) = -g*y(2)+d*y(1)*y(2);
erg(3)= y(2)*erg(1)-y(1)*erg(2);
erg=erg';

