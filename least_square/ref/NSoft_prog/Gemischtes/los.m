%Test von Newton-Fluss mit Eingabe der Startpunkte
%ueber Fadenkeuz
axis([-3 3 -3 3])
opt = odeset('OutputFcn',@odephas2);
for k=1:30
    hold on
    x=ginput(1);
    ode45(@(t,y)newtonfluss(t,y,@kreis),[0,6],x,opt);
end