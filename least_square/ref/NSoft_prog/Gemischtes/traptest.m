% Programm zum Test des G�te der summierten Trapezregel
exakt=2*(exp(1)-1);
for n=1:100
x=linspace(-1,1,n+1);
y=exp(abs(x));
h(n)=2/n;
W(n) = trapz(y)*h(n);
end

Err = W-exakt;
loglog(h,Err)