function erg = JT(x);
%Defines a funcion: x in R3 --> JT(x) in R2 with
% known Jacobian M.
% To be used to test the programm Jakob.
M=[1 2 3; 3 2 1];
erg = M*x;
