function erg = expsin(x)
% Expsin-Testfunktion f�r L�ser nichtinearer Systeme
erg(1) = exp(x(1)^2+x(2)^2)-3;
erg(2)= x(1)+x(2)-sin(3*(x(1)+x(2)));
erg=erg';
