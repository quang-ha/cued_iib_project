x0=[0;2];
opts=odeset('RelTol',1e-2);  %Reduce exactness of integration

[t,y]=ode45(@(t,y)implicit2(t,y,@circle, 0.03),[0,40*pi],x0,opts);

plot(y(:,1),y(:,2));
axis equal;

% Notice that even with a reduced exactness of the integration
% process the approximations tend to converge to the circle.