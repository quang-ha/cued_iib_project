function[J,F] = Jakob(fun,x)
% Approximiert die Jakobimatrix der auf fun übergebenen 
% Funktion an der Stelle x
n=length(x);
ee = sqrt(eps);
F = fun(x);
m=length(F);
for k=1:n
    vari = max(1,abs(x(k)))*ee;
    savex = x(k);
    x(k) = x(k)+vari;
    J(1:m,k)= (feval(fun,x)-F)/vari;
    x(k)=savex;
end
