function erg=zylebene(x)
% Wird Null in Schnitt eines Zylinders erg(1)=0
% und einer Kugel  erg(2)=0
global a
n=[1,1,1];
n=n/norm(n);
v=[1,-1,0];
w=[1,1,-2];
v=v/norm(v);
w=w/norm(w);
erg(1)=(v*x)^2+(w*x)^2-1;
erg(2) = norm(x-[-1,1,-1]')^2-3;

erg=erg';

   

