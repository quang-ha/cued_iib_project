function erg = roundmy(T)
x0=[-1.8592   -0.4964   -0.8533];
opts=odeset('RelTol',1e-2,'OutputFcn',@odephas3);


[t,y]=ode45(@(t,y)implicit2(t,y,@zyllkugel,4),[0,T],x0);
erg = norm(y(end,:)-x0)^2;



