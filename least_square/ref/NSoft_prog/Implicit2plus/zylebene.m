function erg=zylebene(x)
% Beschreibt Schnitt  eines Zylinders (erg(1)) mit einer Ebene (erg(2));
% Eine Verschiebung der Ebene in Normalenrichtung ist �ber den Parameter a
% m�glich. Dessen Wert wird als globale Variable von au�en besetzt.
global a
n=[1,1,1];
n=n/norm(n);
v=[1,-1,0];
w=[1,1,-2];
v=v/norm(v);
w=w/norm(w);
erg(1)=((v*x)^2+(w*x)^2-1); %-2/3*(x(1)^2+x(2)^2+x(3)^2-x(1)*x(2)-x(1)*x(3)-x(2)*x(3)-3/2);
erg(2) = n*x-a;
erg=erg';

   

