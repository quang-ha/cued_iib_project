
x0=[-1.8592   -0.4964   -0.8533];
opts=odeset('RelTol',1e-2,'OutputFcn',@odephas3);


[t,y]=ode45(@(t,y)implicit2(t,y,@zyllkugel,4),[0,9.277],x0);

plot3(y(:,1),y(:,2),y(:,3),'r');
hold on


axis equal;
