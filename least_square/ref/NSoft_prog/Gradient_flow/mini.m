function erg = mini(x);
% Example for which the gradientflow is demonstrated
erg = ((x(1)-1)^2+x(2)^2-2)^2+...
    ((x(1)+1)^2+x(2)^2-2)^2;
