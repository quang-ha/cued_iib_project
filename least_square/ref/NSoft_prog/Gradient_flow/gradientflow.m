function erg = gradientflow(t,x,fun)
% This turnes the function fun into the differential equation
% of the gradient-flow.
[g,f] = Jakob(fun,x);
% Jakob computes (approximations to) general Jacobian matrices
% For real valued functions the Jacobian is just the gradient.
erg=-g';
% The gradient has to be transposed, since differential-equation
% solvers want right-hand sides of ODEs to be column vectors.
% We have to use the negativ gradient, as we want to go DOWNhill