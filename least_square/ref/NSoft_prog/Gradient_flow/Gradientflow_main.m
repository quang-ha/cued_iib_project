%main programme for gradient-Flow-Demo
x=linspace(-2,2,101);
y=x;
[X,Y]= meshgrid(x,y);
Z= ((X-1).^2+Y.^2-2).^2+((X+1).^2+Y.^2-2).^2;
contour(x,y,Z,55);
%contour plots the contour- or level-lines of the
%function mini. We computed these values in Z on a 
% (x,y)-grid
hold on
opt1 = odeset('OutputFcn',@odephas2);
%This option demands the phasediagramme being plotted 
% during integration
[t,y]= ode45(@(t,y)gradientflow(t,y,@mini) , [0,10],[2,2]',opt1);