function [erg] = stein(t,z)
% Ersatzsystem f�r x''=0; y'' = -9.81
% z(1)=x, z(2)=x', z(3)=y, z(4)=y'
%
erg=[z(2); 0; z(4);0] + [0;0;0;-9.81];
