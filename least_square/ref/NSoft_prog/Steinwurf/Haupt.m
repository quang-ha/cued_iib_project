% Programm zur beschreibung der Trajektorie eine geworfenen 
% Steines.
%
% Awa:   x''=0; y'' = -9.81, x(0), y(0), x'(0), y'(o) gegeben.
% Eingabe des Startpunktes 
%
ort = input('geben Sie den Startpunkt als Vektor [x,y] ein:\n');
if isempty(ort)
    ort=[0,4]; %Default-Wert bei fehlender Eingabe
end
v = input('geben Sie die Startgeschwindigkeit [vx,vy] ein:\n');
if isempty(v)
    v=[4,6]; %Default-Wert bei fehlender Eingabe
end

z0 = [ort(1), v(1), ort(2), v(2)]';
%
% Eingabe des Integrationsintervalles
tspan = input('geben Sie das Integrationsintervall [tstart, tende] ein:\n');
if isempty(tspan)
    tspan=[0,2]; %Default-Wert bei fehlender Eingabe
end


%
% Integration jetzt:
opti=odeset('OutputSel', [1 3 2 4], 'OutputFcn','odephas2');
% OutputSel ordnet die Komponenten f�r die outputfunktion  entsprechend des
% nachfolgenden Indexvektors um. Die outpunktfunktionen greifen jeweil auf die 
% ersten so umgeordneten Vektoren zu
[t,z] = ode45(@stein,tspan,z0,opti);

figure(2)
plot(t,z);



