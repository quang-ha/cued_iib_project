function [x,isConverged]=newton_armijo(fun,x0,tol,maxIter)
% Newton-Iteration with Armijo-damping: 
% Successfull end if ||Newton-step|| < tol. isConverged :=true
% Unsucessfull end if  maximal iteration number is reached but
%            still  ||Newton-step|| >= tol. 
%            or if damping does not guerantee reduction of norm(fun)
%            In both cases   isConverged :=false
  %initialize
  error=inf; %set error to infinity
  x=x0;
  isConverged=true;
  iterations=0;
  
  while (( error>tol )&&( isConverged ))
  
    %evaluate the function, the derivate and the vector field
    [dfValue,fValue]=Jakob(fun,x);
    N=-dfValue\fValue;
    
    %Newton-step is good estimate of actual error 
    error=norm(N);
    
    %  Armijo-Line-Search
    StartValue = norm(fValue);
    compare = StartValue;
    x_comp = max(norm(x),1);
    while ((compare >= StartValue)&&(isConverged))
    %update x
       x_n=x+N;
       compare=norm(feval(fun,x_n));
       if norm(N) < 10*eps*x_comp
           isConvergend=false
       end
    N=N/2;
    end
    
    %Except update
    x=x_n;
    
       
    %increase iteration number and check if maxIter is
    %reached
    iterations=iterations+1;
    if iterations==maxIter
      isConverged=false;
    end;
    
  end;
