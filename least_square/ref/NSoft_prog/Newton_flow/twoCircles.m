function erg = twoCircles(x);
erg(1)= (x(1)-1)^2+x(2)^2-2;
erg(2)= (x(1)+1)^2+x(2)^2-2;
erg=erg';
