function erg = Newtonflow(t,x,Fun)
% This turnes the function Fun into the differential equation
% of the Newton-flow.
[J,F] = Jakob(Fun,x);
% Jakob computes (approximations to) general Jacobian matrices
erg=-J\F;

