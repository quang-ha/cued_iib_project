
%main programme for Newton-Flow-Demo

opt1 = odeset('OutputFcn',@odephas2);
%This option demands the phase-diagramme to be plotted 
% during integration
% 1. plot the axes 
plot([-4,4],[0,0],[0,0],[-4,4]);
% 2. plot the solutions 
plot(0,1,'or', 0,-1,'or')
hold on 
% 3. plot several Newton-paths  
for j=-10:10
[t,y]= ode45(@Newtonflow, [0,10],[0.4*j,1e-1]','',@twoCircles);
plot(y(:,1),y(:,2));
end
for j=-10:10
[t,y]= ode45(@Newtonflow, [0,10],[0.4*j,-1e-1]','',@twoCircles);
plot(y(:,1),y(:,2));
end

axis equal;


