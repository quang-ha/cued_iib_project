function [x,y,nonconverged]  = inv_inter(fun,x1,x2,tol,n)
% This routine explaines the idea of Inverse Interpolation.
% In a professional implementation one would not use
% polyval and polyfit but use a direct implementaion of a
% Neville-Aitken-scheme of interpolation
y(1)=fun(x(1));
tolh=tol/2;
z=fun([x(1)-tolh,x(1)+tolh])
nonconverged=(z(1)*z(2)>0)
step =2
while(nonconverged && (step<n+2) )
    y(step)= funct1(x(step));
    x(step+1) =polyval(polyfit(y(1:step),x(1:step),step-1),0);
    z=fun([x(1)-tolh,x(1)+tolh]);
    nonconverged=(z(1)*z(2)>0);
    step=step+1;
end
