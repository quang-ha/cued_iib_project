
%main programme for Fixed-Point-Flow

% 1. plot the axes 
plot([-4,4],[0,0],[0,0],[-4,4]);
% 2. plot the solution  0 
plot(0,0,'or')
hold on 
% 3. plot several F-P-paths  
for j=-10:10
[t,y]= ode45(@fpflow, [0,10],[4,-1.6+0.225*j]','',@fp);
plot(y(:,1),y(:,2));
[t,y]= ode45(@fpflow, [0,10],[-4, 1.5+ 0.225*j]','',@fp);
plot(y(:,1),y(:,2));
[t,y]= ode45(@fpflow, [0,10],[0.4*j, 4]','',@fp);
plot(y(:,1),y(:,2));
[t,y]= ode45(@fpflow, [0,10],[0.4*j,-4]','',@fp);
plot(y(:,1),y(:,2));
end
axis equal;



