%Demo programm to show the vectorfield associated with the iteration
%x_new=fp(x). Whenever a point is entered in the field with red boundary
%this point is drawn together with the line from x to xnew.
% The programm is stopped by entering a point with horizontal component
% larger than 4.
axis([-6 6 -5 5]);
hold on
plot([-5  5 5 -5 -5],[-4 -4 4 4 -4], 'r');
z=1;
while (z<5)
     [x(1),x(2)] =ginput(1);
     z=x(1);
     xn=fp(x);
     plot([x(1),xn(1)],[x(2),xn(2)],'k');
     x=xn;
end




