function erg = fp(x);
% Function for which x with fp(x)=x is  sought.
erg(1)= 0.5*x(1) + 1.2*x(2);
erg(2)= 0.5*x(2);
erg=erg';

