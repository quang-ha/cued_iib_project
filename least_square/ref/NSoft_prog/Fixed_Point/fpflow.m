function erg = fpflow(t,x,Phi)
% This turnes the Fixpoint-vectorfield
% Phi(x) - x belonging to the iteration 
%  x(k+1) := Phi(x(k)) into a differtial equation.
erg= Phi(x) - x;


