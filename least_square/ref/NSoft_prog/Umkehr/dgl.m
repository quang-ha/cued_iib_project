function erg = dgl(t,y,lambda)
erg=[y(2); -lambda*exp(y(1))];
    