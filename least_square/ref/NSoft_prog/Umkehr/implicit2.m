 function res = implicit2(t,y,fun,alpha)
% Assumption: Let fun in C^2(R^n,R^(n-1)) and Rank (Jacobian(fun)) = n-1
% in the (say convex and open ) y-region  R we are interested in.
% If there is a y* in R with f(y*)=0, the equation fun(y)= 0 defines 
% a curve  in R through y*.
%
% 1. For alpha =0 the programme defines a vectorfield (to be used as 
% right-hand side of  an ordinary differential equation) such that
% the integration of this  differential equation follows this curve 
% if started at y0 on the curve, i.e. if fun(y0)=0.
% EXAMPLE
%        function res = circle(y)
%        % defines unit circle in R^2
%        res = y(1)^2+y(2)^2 -1;
%
%        T=10; y0=[1,0]'; [t,y] = ode45(@implicit2,[0,T],y0,'', @circle,0)
%
% 2. If fun(y0) \ne 0 and alpha =0, the same integration 
%        T=10; y0=[2,0]'; [t,y] = ode45(@implicit2,[0,T],y0,'', @circle,0)
% follows the "parallel curve" through y0 defined by fun(y) = fun(y0).
% For the example this would be the circle around the origin with radius
% two.
%
%3. If fun(y0) \ne 0 and alpha >0, the  integration 
%     T=10; y0=[2,0]'; [t,y] = ode45(@implicit2,[0,T],y0,'', @circle,alpha)
% would get a "Newton-bias" towards the curve, which becomes more 
% intensive with a larger alpha. 
%
% Remark: In order to prevent the integration from leaving the solutin 
% curve due to integration errors of the numerical integration procedure, 
% it is recomended to use always a positive alpha-value, which would always
% restore fun(y)=0. One should NOT use to large an alpha, or use ode23s
% instead of ode45, since large alpha will cause stiffness of the problem.

[J,F] = Jakob(fun,y);
N=null(J);
JJ=[J;N'];
if det(JJ)<0
    N=-N;
end
JJ=[J;N'];
res = JJ\[-alpha*F;1];

% Notice1
% res ist a superposition res = res1+abs(alpha)*res2 of res1=N, which is
% a solution of 
%          JJ res1 = [zeros(size(F)); 1]
% and a Newton-type step which leeds back to the solution manifold of
% fun(y)=0:
%
% JJ res2 = [-F; 0]
% Notice that res2 fulfills both
%  J res2 = -F,
% wich is the linearization of fun(y)=0)
% and 
%  N' res2 = 0
% which complements the matrix J in the previous equation to become a
% nonsingular square equation. The equation N' res2 =0 chooses amonf all
% solutions of J res2 = -F that one, which preeds ohogonally to the tangent
% field N.

