function res = davidenko(y,x, fun)
%Assumption: z= (x,y), x is m-dimensional, y is 1D
%            fun(x,y) is m dimensional; fun_x regular
%            Programm = Davidenko; except that calculation
%            of Jacobian is done by subprogram "jakob"
z=[x;y];
m=length(x);
[J,F]=Jakob(fun,z);
res = -J(:,1:m)\J(:,m+1);










