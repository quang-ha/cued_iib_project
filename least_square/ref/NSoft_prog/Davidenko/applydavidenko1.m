%Apply Davidenko to the circle equation 
% x^2+y^2-1 =0, using y as the parameter; i.e. assuming that the solution
% set is parametrized  as (x(y),y), y in [0,1).
% Within the routine the equation x(y)^2 +y^2 -1 is differentiated with
% respect to y, and the derivative of x(y) is handed over to ode45 in the
% next statement:
[y,x]= ode45(@davidenko, [0,1], 1,'', @circle);
% Since the coefficient x(y) of x'(y) in the Davidenko-equation goes to
% zero as y approaches 1, the Davidenko-Equation becomes a singular
% equation and this the integration stepwidth is reduced further and
% further.
% This is shown by plotting the stepwidth in a semilogarithmic plot:
h = diff(y);
semilogy(h,'ob')
figure
% The result of the integration is shown in Figure 2.
plot(x,y,'b');
hold on 
%If one startes the integration with a starting point(y0,x0) not on the
%solution circle
[y,x]= ode45(@(t,y)davidenko(t,y,@circle), [0,2], 2 );
%"Davidenko" does not notice this but solves x^2 + y^2 = x0^2+ y0^2 instead
%since the constant 1 in x^2+y^2 -1 drops out during deriving x'(y) by
%differentiation.
plot(x,y,'r');
axis equal
