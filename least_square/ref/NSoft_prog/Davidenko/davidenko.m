function res = davidenko(y,x, fun)
% DAVIDENKO-equation for implicit function.
% Program with integrated computation of Jacobian.
%Assumption: z= (x,y), x is m-dimensional, y is 1D
%            fun(x,y) is m dimensional; fun_x regular
%            Program finds x(y) with f(x(y),y)=0.
z=[x;y];
n=length(z);
F=feval(fun,z);
m=length(F);
for k=1:n
    h= max(abs(z(k)),1)*sqrt(eps);
    zstore=z(k);
    z(k)=z(k)+h;
    Fd=feval(fun,z);
    J(1:m,k)=(Fd-F)/h;
    z(k)=zstore;
end
res = -J(:,1:m)\J(:,m+1);










