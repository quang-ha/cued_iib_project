function[erg] = langford(x,y,ff,alpha, beta, lambda, omega,rho, epsilon)
erg(1) = (y(3)-beta)*y(1)- omega*y(2);
erg(2) = omega*y(1) + (y(3)-beta)*y(2);
erg(3)= lambda + alpha*y(3) - (y(3)^3)/3 - (y(1)^2+y(2)^2)*(1+rho*y(3)) + eps*y(3)*y(1)^3;
erg=erg';


%Vor Aufruf setzen
%options = odeset('OutputFcn',@odephas3)

% Aufruf
%ODE45(@langford,[0 200], [1 2 3]',options,1, 0.7, 0.6, 3.5, 0.25, 0.06)