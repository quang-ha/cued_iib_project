function[erg] = waerme(t,u,c,lambda,quelle,randl,randr)
% Linienmethode für die Wärmeleitungsgleichung  
% u_t = c*u_xx + quelle(t,u);
% u(0,t) = randl(t); u(1,t) = randr(t);
%
% Anfangsbedingungen werden direkt als Anfangswerte in 
% den ODE-Integrator eingegeben 
n=length(u);
h=1/(n+1);
edh2=1/h^2;
erg= -2*u+[u(2:n);0]+[0;u(1:n-1)];
erg(1)=erg(1)+randl(t);
erg(n)=erg(n)+randr(t);
erg=erg*edh2;
%add=feval(quelle,t,u)
erg = erg+ quelle(t,u,lambda);


