%Anwendung der Waermeleitungsprogramme
% Wie Anwendung1 nur mit gro�em Lambda
c=1;
lambda=10;
u0= zeros(19,1);
x=[1:19]'/20;

[t,y]= ode23s(@(t,u)waerme(t,u,c,lambda,@reaktio,@dirichlet0,@dirichlet0),[0,0.123], u0);


yvoll=[dirichlet0(t),y,dirichlet0(t)];
xvoll=[0;x;1];

mesh(xvoll,t,yvoll);


