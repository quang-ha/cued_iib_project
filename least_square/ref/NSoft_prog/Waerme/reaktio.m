function erg = reaktio(t,u,lambda);
% Konkretisierung des Quelltermes
% Exotherme Reaktion
erg = lambda*exp(u);

