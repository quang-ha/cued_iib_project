%Anwendung der Waermeleitungsprogramme

u0= zeros(19,1); % Anfangswerte
x=[1:19]'/20;    % innere Raum-Punkte
c=1; % Wärmeleitkoeffizient
lambda =1; % Reaktionskoeffizient
[t,y]= ode23s(@(t,u)waerme(t,u,c,lambda,@reaktio,@dirichlet0,@dirichlet0),[0,20], u0);
% reaktio beschreibt als Quelle einen Reaktionsterm; dirichlet0 gibt
% Null-Randbedingungen vor
yvoll=[dirichlet0(t),y,dirichlet0(t)];  % Anfügen der Randwerte 
xvoll=[0;x;1]; % Raumpunkte mit Randpunkten versehen

mesh(xvoll,t,yvoll);

