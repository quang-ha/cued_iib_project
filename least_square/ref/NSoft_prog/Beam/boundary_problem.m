% Main programme to solve the beam-boundary-value problem
guess = [-1,0]'; %Inial guess  for the initial values in(1) and in(2)
                 % For nonlinear problem good guesses are essential.
                 % Since the differential equations right hand side "-x"
                 % does not depend on "y", the problem is linear. Any guess
                 % will do.
 out = fsolve(@shoot, guess)
 %fsolve computes those values for in(1) and in(2), which will 
 % guarantee that the boundary values on the right hand side are satisfies,
 % which means that the values which shoot returns are zero.
 
 % Having computed these values we can intergrate the differential equation
 % again with the correct initial values 
 [t,z] = ode45(@beam,[0,3],[0,1,out(1),out(2)]);
 % Now the colums of z contain; 1. column: values of y at arguments in t
                              % 2. Column: values of y'
                              % 3. column: values of y''
                              % 4. column: values of y'''
                             
% Let us plot y
plot(t,z(:,1))
