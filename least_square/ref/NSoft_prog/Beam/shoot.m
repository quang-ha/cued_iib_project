function res = shoot(in)
%Shooting-programm to solve boundary value problem
%y''''(x) = -x;  y(0)=0, y'(0)=1, y(3)=0, y'(3)=1;
% Differential equation is describes by  beam.m
%in(1)= second deriv of y at t=0
%in(2)= third deriv of y at t=0

[x,y]= ode45(@beam,[0,3], [0,1,in(1),in(2)]);
res(1)=y(end,1);  res(2)=y(end,2)-1;
res=res';
