function erg = beam(x,y)
% Describes y'''' = -x;
erg(1)=y(2);
erg(2) = y(3);
erg(3)= y(4);
erg(4)= -x;
erg=erg';

% equivalently one may wright
% erg=[y(2);y(3);y(4);x];
