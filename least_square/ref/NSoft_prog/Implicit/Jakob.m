function [J,F] = Jakob(fun, x)
n=  length(x);
s=sqrt(eps);
F=fun(x);
m= length(F);
for j=1:n
    R=x(j);
    h= max(1,abs(x(j)))*s;
    x(j) =x(j)+h;
    J(1:m,j) = (fun(x)-F)/h;
    x(j)=R;
end;