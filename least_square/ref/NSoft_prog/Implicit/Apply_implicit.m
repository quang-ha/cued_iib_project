x0=[0;1];
opts=odeset('RelTol',1e-2); %Reduce the quality of the integrator.
% This results in drifting away from the unit circle during integration. 

[t,y]=ode45(@(t,y)implicit(t,y,@circle),[0,20*pi],x0,opts);

plot(y(:,1),y(:,2));
axis equal;
