function res = implicit(t,y,fun)
% Assumption: Let fun in C^2(R^n,R^(n-1)) and Rank (Jacobian(fun)) = n-1
% in the (say convex and open ) y-region  R we are interested in.
% If there is a y* in R with f(y*)=0, the equation fun(y)= 0 defines 
% a curve  in R through y*.
%
% 1. The programme defines a vectorfield (to be used as 
% right-hand side of  an ordinary differential equation) such that
% the integration of this  differential equation follows this curve 
% if started at y0 on the curve, i.e. if fun(y0)=0.
% EXAMPLE
%        function res = circle(y)
%        % defines unit circle in R^2
%        res = y(1)^2+y(2)^2-1;
%
%        T=10; y0=[1,0]'; [t,y] = ode45(@implicit,[0,T],y0,'', @circle)
%
% 2. If fun(y0) \ne 0 and alpha =0, the same integration 
%        T=10; y0=[2,0]'; [t,y] = ode45(@implicit,[0,T],y0,'', @circle,0)
% follows the "parallel curve" through y0 defined by fun(y) = fun(y0).
% For the example this would be the circle around the origin with radius
% two.

[J,F] = Jakob(fun,y);
n=null(J);
if det([J;n'])<0
    n=-n;
end
res =n;





