%% SCRIPT TO GENERATE MATRIX WITH SPECIFIC RANK
% m = rows, n = column, k = desired rank with k<min(m,n)
m = 50;
n = 50; 
k = 50;
IMAX = 5.0;

% Get the matrix
A = randi(IMAX,m,k)*randi(IMAX,k,n);

% Now normalise it
max_A = max(A,[],1);
min_A = min(A,[],1);
A_norm = ((repmat(max_A,m,1)-A)./repmat(max_A-min_A,m,1));
