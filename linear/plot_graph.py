import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from mpltools import annotation
import numpy as np

n = [10, 50, 100, 500, 1000, 5000, 10000]
action = [0.000768, 0.000812, 0.000890, 0.001282, 0.001746, 0.006065, 0.011171]
n_1 = [10, 50, 100, 500, 1000, 5000]
matrix = [0.004866, 0.018380, 0.035637, 0.261264, 0.973890, 75.153351]

# slope, intercept = np.polyfit(np.log(n_1),np.log(matrix),deg=1)
# print slope
# exit(1)

# action slope = 0.390324937661
# matrix slope = 1.49522369253
# # Plotting and compare the result
plt.figure(figsize=(12,9))
plt.loglog(n_1, matrix, label='Full Matrix', marker='o', linewidth=2.0)
plt.loglog(n, action, label='Matrix-Action', marker='o', linewidth=2.0)
plt.legend(loc=2)
plt.grid()
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tick_params(axis='both', which='minor', labelsize=15)
plt.xlabel('Number of nodes', fontsize=15)
plt.ylabel('Time taken (seconds)', fontsize=15)
annotation.slope_marker((700, 1e-3), (0.39, 1),
						size_frac = 0.15,
						pad_frac = 0.1,
                        text_kwargs={'color': 'green', 'fontsize': 15},
                        poly_kwargs={'edgecolor': 'green', 'facecolor': 'green', 'fill': True})
annotation.slope_marker((300, 0.05), (1.50, 1),
						size_frac = 0.15,
						pad_frac = 0.1,
                        text_kwargs={'color': 'blue', 'fontsize': 15},
                        poly_kwargs={'edgecolor': 'blue', 'facecolor': 'blue', 'fill': True})
plt.tight_layout()                       
plt.show()
