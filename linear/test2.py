from dolfin import *
import scipy.sparse.linalg
import scipy.sparse
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

#### Let's try and capture the sparse matrix into Scipy
parameters['linear_algebra_backend'] = 'Eigen'

# Boundary of the problem
xmin = 1.0
xmax = 5.0

# Create mesh and define function space
n = 300
m = 7
mesh = IntervalMesh(n-1, xmin, xmax)
V = FunctionSpace(mesh, "Lagrange", 1)

########### FORWARD PROBLEM - SOLVING ####################
# Define Dirichlet boundary
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Define boundary condition
bc = DirichletBC(V, Constant(0.0), DirichletBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f_exp = Expression("x[0]*x[0]")
a = inner(grad(u), grad(v))*dx 
L = f_exp*v*dx 

# Compute solution
u = Function(V)
# solve(a==L, u, bc)
# plot(u)
# interactive()
# exit(1)

########### INVERSE PROBLEM - SOLVING ####################
# Get the sparse matrix from a
A = assemble(a)
bc.apply(A)
Asp = as_backend_type(A).sparray().tocsc()
# convert to Compressed Sparse Column format
# To increase efficiency (??!)
Asp_inv = scipy.sparse.linalg.inv(Asp)

# Compute the sparsity collection
# Get places of data
coord_obs = np.linspace(0, n-1, m)
coord_obs = np.round(coord_obs)
data = np.ones(m)
row = np.linspace(0, m-1, m)
# Now generate the sparsity obs matrix
S = scipy.sparse.csc_matrix((data, (row, coord_obs)), shape=(m,n))

# And the Z matrix is now
Z = S.dot(Asp_inv)

########### INVERSE PROBLEM - SOLVING ####################
# Generate nosie and prior covariance matrix
sigma_noise = 0.3
sigma_prior = 0.01
Gamma_noise_inv = scipy.sparse.diags([1/sigma_noise**2], [0], shape=(m,m)).tocsc()
Gamma_prior_inv = scipy.sparse.diags([1/sigma_prior**2], [0], shape=(n,n)).tocsc()

# Generate prior knowledge
f_prior = (1.15*np.linspace(xmin, xmax, n))**2

# Get the noisy fake data
u_noise_array = u.vector()[:] + 2.0*np.random.normal(0, sigma_noise, n)
u_noise_obs = S.dot(u_noise_array)

# Now generate the Hessian and the right hand side for linear algebra
Q = Z.T.dot(Gamma_noise_inv.dot(Z)) + Gamma_prior_inv
b = (Gamma_noise_inv.dot(Z)).T.dot(u_noise_obs) + Gamma_prior_inv.dot(f_prior)
# And now let's solve for the inverse solution
[f_result, tol] = scipy.sparse.linalg.cg(Q, b, tol=1e-8) 

# Let's try and see
f_ref = Function(V)
f_ref.interpolate(f_exp)

Q_inv = scipy.sparse.linalg.inv(Q.tocsc())
# Get diagonal component. In 1D that's what matter
Q_inv_diag = Q_inv.diagonal()

############### MATRIX ACTION ###########################
############### for Qx, which is Qf######################
def Qmatrix_action(K, x, Gprior, Gnoise, Spc, u):
	# Here x is a numpy vector and A is EigenMatrix
	# C is the middle matrix
	# Create Eigen Solver
	solver = EigenLUSolver("sparselu")
	solver.set_operator(K)
	# Store final product of Qx
	y = np.zeros(n)
	y += Gamma_prior_inv.dot(x)
	# Create function
	x2 = Vector(u.vector())
	x2[:] = x
	x1 = Vector()
	# Now solve
	solver.solve(x1, x2)
	# This be the middle matrix
	C = Spc.T.dot(Gnoise.dot(Spc))
	# Now solve the next problem with
	x2[:] = C.dot(x1.array())
	solver.solve_transpose(x1, x2)
	y += x1.array()
	return y

# x = np.ones(n)
# y = Qmatrix_action(A, x, Gamma_prior_inv, Gamma_noise_inv, S, u)
# print y
# # Compare with the matrix vector product
# print Q.dot(x)
# print np.linalg.norm(y - Q.dot(x))

################### CG MATRIX ACTION ###################
# Initilise the starting point
x0 = np.zeros(n)
y = Qmatrix_action(A, x0, Gamma_prior_inv, Gamma_noise_inv, S, u)
r0 = b - y
p0 = r0
tol = 1e-8
k = 0

# Now let's do the conjugate gradient
while True:
	y = Qmatrix_action(A, p0, Gamma_prior_inv, Gamma_noise_inv, S, u)
	alpha = (r0.dot(r0))/(p0.dot(y))
	# Update residual and x
	x1 = x0 + alpha*p0
	r1 = r0 - alpha*y
	# Check for break
	if np.linalg.norm(r1) < tol:
		break
	# Update search direction
	beta = (r1.dot(r1))/(r0.dot(r0))
	p1 = r1 + beta*p0

	# Increase the step and update
	k += 1
	x0, r0, p0 = x1, r1, p1

# print x1
# print f_result
# print np.linalg.norm(x1 - f_result)
plt.plot(x1, label='Matrix Action')
plt.plot(f_result, label='Full Matrix')
plt.grid()
plt.legend()
plt.show()