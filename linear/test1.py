from dolfin import *
import scipy.sparse.linalg
import scipy.sparse
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

#### Let's try and capture the sparse matrix into Scipy
parameters['linear_algebra_backend'] = 'Eigen'

# Boundary of the problem
xmin = 1.0
xmax = 5.0

# Create mesh and define function space
n = 201 # Number of points on the mesh
m = 7 # Number of observation
mesh = IntervalMesh(n-1, xmin, xmax)
V = FunctionSpace(mesh, "Lagrange", 1)

########### FORWARD PROBLEM - SOLVING ####################
# Define Dirichlet boundary
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, DirichletBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("x[0]*x[0]")
a = inner(grad(u), grad(v))*dx
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# # Plot solution	
# plot(u)
# interactive()

# Get the sparse matrix from a
A = assemble(a)
bc.apply(A)
Asp = as_backend_type(A).sparray().tocsc()
# convert to Compressed Sparse Column format
# To increase efficiency (??!)
Asp_inv = scipy.sparse.linalg.inv(Asp)

# Compute the sparsity collection
# Get places of data
coord_obs = np.linspace(0, n-1, m)
coord_obs = np.round(coord_obs)
data = np.ones(m)
row = np.linspace(0, m-1, m)
# Now generate the sparsity obs matrix
S = scipy.sparse.csc_matrix((data, (row, coord_obs)), shape=(m,n))

# And the Z matrix is now
Z = S.dot(Asp_inv)

########### INVERSE PROBLEM - SOLVING ####################
# Generate nosie and prior covariance matrix
sigma_noise = 0.5
sigma_prior = 0.01
Gamma_noise_inv = scipy.sparse.diags([1/sigma_noise**2], [0], shape=(m,m)).tocsc()
Gamma_prior_inv = scipy.sparse.diags([1/sigma_prior**2], [0], shape=(n,n)).tocsc()

# Generate prior knowledge
f_prior = (1.15*np.linspace(xmin, xmax, n))**2

# Get the noisy fake data
u_noise_array = u.vector()[:] # + 25.0*np.random.normal(0, sigma_noise, n)
u_noise_obs = S.dot(u_noise_array)
# plt.plot(S.dot(mesh.coordinates()), u_noise_obs)
# plt.plot(mesh.coordinates(), u.vector()[:])
# plt.show()
# exit(1)

# Now generate the Hessian and the right hand side for linear algebra
H_data = Z.T.dot(Z)
Q = Z.T.dot(Gamma_noise_inv.dot(Z)) + Gamma_prior_inv
b = (Gamma_noise_inv.dot(Z)).T.dot(u_noise_obs) + Gamma_prior_inv.dot(f_prior)
# Q = Z.T.dot(Z)
# b = Z.T.dot(u_noise_obs)
# And now let's solve for the inverse solution
[f_result, tol] = scipy.sparse.linalg.cg(Q, b, tol=1e-8) 

# # Let's try and see
f_ref = Function(V)
f_ref.interpolate(f)
# # plot(f_ref, interactive=True)

# plt.plot(f_result)
# plt.plot(f_ref.vector().array())
# plt.show()
# exit(1)

Q_inv = scipy.sparse.linalg.inv(Q.tocsc())
# Get diagonal component. In 1D that's what matter
Q_inv_diag = Q_inv.diagonal()

################### CG INVERSE HESSIAN ###################
# Initilise the starting point
x0 = np.zeros(n)
r0 = b - Q.dot(x0)
p0 = r0
tol = 1e-8
k = 0
# Create matrix to store these guys
P = (p0/np.linalg.norm(p0)).T
V = (r0/np.linalg.norm(r0)).T

# Now let's do the conjugate gradient
while True:
	alpha = (r0.dot(r0))/(p0.dot(Q.dot(p0)))
	# Update residual and x
	x1 = x0 + alpha*p0
	r1 = r0 - alpha*Q.dot(p0)
	# Check for break
	if np.linalg.norm(r1) < tol:
		break
	# Update search direction
	beta = (r1.dot(r1))/(r0.dot(r0))
	p1 = r1 + beta*p0

	# Increase the step and update
	k += 1
	x0, r0, p0 = x1, r1, p1
	P = np.column_stack((P, (p0/ np.linalg.norm(p0)).T))
	V = np.column_stack((V, (r0/ np.linalg.norm(r0)).T))

# print P.T.dot(Q.dot(P))
# exit(1)

# Now build the approximation
D = P.T.dot(Q.dot(P))
eta = 1/(sigma_prior**2)
# Q_inv_approx = P.dot(np.linalg.inv(D + eta*P.T.dot(P)).dot(P.T)) + (1/eta)*(scipy.sparse.identity(n) - V.dot(V.T))
Q_inv_approx = P.dot(np.linalg.inv(D).dot(P.T)) + (1/eta)*(scipy.sparse.identity(n) - V.dot(V.T))
# Get the approx
Q_inv_approx = scipy.sparse.csc_matrix(Q_inv_approx)
Q_approx = scipy.sparse.linalg.inv(Q_inv_approx)

print scipy.sparse.linalg.norm(Q_inv - Q_inv_approx)
# exit(1)
# Q_inv_approx_diag = Q_inv.diagonal()

#################### MATPLOTLIB ##########################
# Get the mesh coordinate
mesh_c = mesh.coordinates()

####### PLOT EIGEN VALUES OF data matching Hessian #######
# plt.figure(figsize=(12,9))
# Hdata = Z.T.dot(Gamma_noise_inv.dot(Z))
# vals, vecs = scipy.sparse.linalg.eigs(Hdata, k=50)
# plt.gca().ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
# plt.plot(vals, linewidth=1.5, marker='o')
# plt.ylabel('Eigenvalues', fontsize=15)
# plt.xlabel('Number of eigenvalues', fontsize=15)
# plt.grid()
# plt.show()
# exit(1)

# # Let's try and extract the sparse observation
# spr = S.dot(u.vector()[:])
# plt.figure(figsize=(12,9))
# plt.plot(mesh_c, u.vector()[:], label='Exact data')
# plt.plot(S.dot(mesh_c), u_noise_obs, label='Noisy obs', marker='o', linewidth=2.0)
# plt.plot(S.dot(mesh_c), spr, label='Sparse obs', marker='p')
# plt.legend()
# plt.grid()
# plt.tick_params(axis='both', which='major', labelsize=15)
# plt.tick_params(axis='both', which='minor', labelsize=15)
# plt.xlabel('Mesh coordinates', fontsize=15)
# plt.ylabel(r'Values of $u$', fontsize=15)
# plt.show()

# # # Plotting and compare the result
# plt.figure(figsize=(12,9))
# plt.plot(mesh_c, f_prior, label='Prior guess', linewidth=2.0)
# plt.plot(mesh_c, f_ref.vector()[:], label='Exact solution', linewidth=2.0)
# plt.plot(mesh_c, f_result, label='Result', linewidth=2.0)
# plt.legend()
# plt.grid()
# plt.tick_params(axis='both', which='major', labelsize=15)
# plt.tick_params(axis='both', which='minor', labelsize=15)
# plt.xlabel('Mesh coordinates', fontsize=15)
# plt.ylabel(r'Values of $f$', fontsize=15)
# plt.show()

# # Plotting the inverse Hessian
# plt.figure(figsize=(12,9))
# plt.plot(mesh_c, (sigma_prior)*np.ones(n), label='Prior', linewidth=1.5)
# plt.plot(mesh_c, np.sqrt(Q_inv_diag), label='Exact??', linewidth=1.5)
# # plt.plot(mesh_c, np.sqrt(Q_inv_approx_diag), label='Approx', linewidth=1.5)
# sigma_post = (sigma_prior**-1 + sigma_noise**-1)**-1
# # plt.plot(mesh_c, (sigma_post)*np.ones(n), label='Exact')
# plt.gca().yaxis.set_major_formatter(mtick.ScalarFormatter(useMathText=True))
# plt.gca().ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
# # plt.ylim([0.9*(sigma_prior), 1.1*(sigma_prior)])
# plt.tick_params(axis='both', which='major', labelsize=15)
# plt.tick_params(axis='both', which='minor', labelsize=15)
# plt.xlabel('Mesh coordinates', fontsize=15)
# plt.ylabel(r'Diagonal elements of $\mathbf{\Gamma}_{\mathrm{post}}$', fontsize=15)
# plt.legend()
# plt.grid()
# plt.show()
# print np.linalg.norm(np.sqrt(Q_inv_diag) - np.sqrt(np.diag(Q_inv_approx)))

# Plot the colour bar - not truly effective...
plt.figure(figsize=(12,9))
plt.imshow(Q_inv.todense(), interpolation='none', cmap='binary')
plt.title(r'Colour map of Exact $\mathbf{\Gamma}_{\mathrm{post}}$')
plt.colorbar(format='%.e')
plt.show()

plt.figure(figsize=(12,9))
plt.imshow(Q_inv_approx.todense(), interpolation='none', cmap='binary')
plt.title(r'Colour map of Approximated $\mathbf{\Gamma}_{\mathrm{post}}$')
plt.colorbar(format='%.e')
plt.show()

##################### ANALYSIS OF Q #####################

# Try and get the condition number of Q
# Try and get the svd
# [UQ, sQ, VQT] = scipy.sparse.linalg.svds(Q, k=100)
	# print np.real(sQ[-1])/np.real(sQ[0])

# Now get the svd of the approx
# [UQa, sQa, VQaT] = scipy.sparse.linalg.svds(Q_approx, k=100)
# print 1.0/sQa
# print sQ

# # Now get the svd of the inverse approx
# [UQa_i, sQa_i, VQa_iT] = scipy.sparse.linalg.svds(Q_inv_approx, k=60)
# print sQa_i
# print 1.0/sQ

# plt.semilogy(1.0/np.sqrt(sQ))
# plt.semilogy(1.0/np.sqrt(sQa))
# plt.ylim([5e-3, 2e-2])
# plt.grid()
# plt.show()
# print np.real(WQ_a[0])/np.real(WQ_a[-1])

# # Try and get the eig values of H_data
# [WH, VH] = scipy.sparse.linalg.eigs(H_data, k=50)
# print np.real(WH[0])/np.real(WH[-1])

# print scipy.sparse.linalg.norm(Q_inv - Q_inv_approx)
# plt.plot(np.sqrt(Q_inv.diagonal()))
# plt.plot(np.sqrt(Q_inv_approx.diagonal()))
# plt.show()

