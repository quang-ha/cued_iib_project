from dolfin import *
import scipy.sparse.linalg
import scipy.sparse
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

#### Let's try and capture the sparse matrix into Scipy
parameters['linear_algebra_backend'] = 'Eigen'

# Boundary of the problem
xmin = 1.0
xmax = 10.0

# Create mesh and define function space
n = 201 # Number of points on the mesh
m = 51 # Number of observation
mesh = IntervalMesh(n-1, xmin, xmax)
# Get coordinates array
mesh_c = mesh.coordinates()
# For 1D hmin = hmax
cell_size = mesh.hmin()

V = FunctionSpace(mesh, "Lagrange", 1)

########### FORWARD PROBLEM - SOLVING ####################
# Define Dirichlet boundary
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, DirichletBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("x[0]*x[0]")
a = inner(grad(u), grad(v))*dx
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# # Plot solution	
# plot(u)
# interactive()
# exit(1)

# Get the sparse matrix from a
K = assemble(a)
bc.apply(K)
Ksp = as_backend_type(K).sparray().tocsc()
# convert to Compressed Sparse Column format
# To increase efficiency (??!)
Ksp_inv = scipy.sparse.linalg.inv(Ksp)

# Compute the sparsity collection
# Get places of data
coord_obs = np.linspace(0, n-1, m)
coord_obs = np.round(coord_obs)
data = np.ones(m)
row = np.linspace(0, m-1, m)
# Now generate the sparsity obs matrix
S = scipy.sparse.csc_matrix((data, (row, coord_obs)), shape=(m,n))

# And the Z matrix is now
Z = S.dot(Ksp_inv)

########### INVERSE PROBLEM - SOLVING ####################
# Generate nosie and prior covariance matrix
sigma_noise = 1.0
sigma_prior = 1.0
Gamma_noise_inv = scipy.sparse.diags([1/sigma_noise**2], [0], shape=(m,m)).tocsc()
Gamma_prior_inv = scipy.sparse.diags([1/sigma_prior**2], [0], shape=(n,n)).tocsc()

# Generate prior knowledge
f_prior = 1.15*(np.linspace(xmin, xmax, n))**2

# Get the noisy fake data
u_noise_array = u.vector()[:] + 2.0*np.random.normal(0, sigma_noise, n)
u_noise_obs = S.dot(u_noise_array)

# plt.plot(S.dot(mesh_c), u_noise_obs)
# plt.plot(S.dot(mesh_c), u.vector()[:])
# plt.show()
# exit(1)

# Now generate the Hessian and the right hand side for linear algebra
H_data = Z.T.dot(Z)
Q = Z.T.dot(Gamma_noise_inv.dot(Z)) + Gamma_prior_inv
b = (Gamma_noise_inv.dot(Z)).T.dot(u_noise_obs) + Gamma_prior_inv.dot(f_prior)
# Q = Z.T.dot(Z)
# b = Z.T.dot(u_noise_obs)
# And now let's solve for the inverse solution
[f_result, tol] = scipy.sparse.linalg.cg(Q, b, tol=1e-8) 
# Multiple factor, for 1D hmin = hmax
f_result *= (1.0/cell_size)

# # # Let's try and see
# # # plot(f, mesh, interactive=True)
# f_ref = Function(V)
# f_ref.interpolate(f)
# plt.plot(mesh_c[10:150], f_result[10:150])
# plt.plot(mesh_c[10:150], f_ref.vector().array()[10:150])
# plt.show()

########## CONDITION NUMBER ########################
[u, s, vT] = scipy.sparse.linalg.svds(Q, k=50)
print s[-1]/s[0]

import csv
with open('test.csv', 'wb') as f:
    writer = csv.writer(f)
    for val in s:
        writer.writerow([val])
# plt.plot(s)
# plt.show()