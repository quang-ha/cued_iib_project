import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from mpltools import annotation
import numpy as np

n = [100, 2500, 10000, 40000]
action = [0.001365, 0.024678, 0.230169, 1.172104]
n_1 = [100, 2500]
matrix = [0.036282, 4.373200]

# slope, intercept = np.polyfit(np.log(n_1),np.log(matrix),deg=1)
# print slope
# exit(1)

# action slope = 1.13247353908
# matrix slope = 1.48869630137
# # Plotting and compare the result
plt.figure(figsize=(12,9))
plt.loglog(n_1, matrix, label='Full Matrix', marker='o', linewidth=2.0)
plt.loglog(n, action, label='Matrix-Action', marker='o', linewidth=2.0)
plt.legend(loc=2)
plt.grid()
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tick_params(axis='both', which='minor', labelsize=15)
plt.xlabel('Number of nodes', fontsize=15)
plt.ylabel('Time taken (seconds)', fontsize=15)
annotation.slope_marker((2e3, 1.25e-2), (1.13, 1),
						size_frac = 0.15,
						pad_frac = 0.1,
                        text_kwargs={'color': 'green', 'fontsize': 15},
                        poly_kwargs={'edgecolor': 'green', 'facecolor': 'green', 'fill': True})
annotation.slope_marker((3e2, 0.12), (1.49, 1),
						size_frac = 0.15,
						pad_frac = 0.1,
                        text_kwargs={'color': 'blue', 'fontsize': 15},
                        poly_kwargs={'edgecolor': 'blue', 'facecolor': 'blue', 'fill': True})
plt.autoscale(enable=True, axis='both', tight=None)                      
plt.show()
