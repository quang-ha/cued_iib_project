from dolfin import *
import scipy.sparse.linalg
import scipy.sparse
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

import time

def main(matrix_free_time):
	# Form compiler options
	parameters["form_compiler"]["optimize"]     = True
	parameters["form_compiler"]["cpp_optimize"] = True
	parameters["form_compiler"]["representation"] = "quadrature"

	#### Let's try and capture the sparse matrix into Scipy
	parameters['linear_algebra_backend'] = 'Eigen'

	# Boundary of the problem
	xmin = 1.0
	xmax = 5.0

	# Create mesh and define function space
	N = 99
	M = int(0.2*N)
	n = (N+1)**2
	m = (M+1)**2
	mesh = UnitSquareMesh(N, N)
	V = FunctionSpace(mesh, "Lagrange", 1)

	########### FORWARD PROBLEM - SOLVING ####################
	# Define Dirichlet boundary
	class DirichletBoundary(SubDomain):
	    def inside(self, x, on_boundary):
	        return on_boundary

	# Define boundary condition
	bc = DirichletBC(V, Constant(0.0), DirichletBoundary())

	# Define variational problem
	u = TrialFunction(V)
	v = TestFunction(V)
	f_exp = Expression("x[0]*x[0]")
	a = inner(grad(u), grad(v))*dx 
	L = f_exp*v*dx 

	# Compute solution
	u = Function(V)
	# solve(a==L, u, bc)
	# plot(u)
	# interactive()
	# exit(1)

	########### INVERSE PROBLEM - SOLVING ####################
	# Generate nosie and prior covariance matrix
	sigma_noise = 0.3
	sigma_prior = 0.01
	Gamma_noise_inv = scipy.sparse.diags([1/sigma_noise**2], [0], shape=(m,m)).tocsc()
	Gamma_prior_inv = scipy.sparse.diags([1/sigma_prior**2], [0], shape=(n,n)).tocsc()

	# Compute the sparsity collection
	# Get places of data
	coord_obs = np.linspace(0, n-1, m)
	coord_obs = np.round(coord_obs)
	data = np.ones(m)
	row = np.linspace(0, m-1, m)
	# Now generate the sparsity obs matrix
	S = scipy.sparse.csc_matrix((data, (row, coord_obs)), shape=(m,n))

	# Get the sparse matrix from a
	A = assemble(a)
	bc.apply(A)
	########### THE MATRIX TINGY ########################
	# Generate a temp matrix
	x = np.ones(n)
	### TIMING STARTS HERE
	t0 = time.time()
	Asp = as_backend_type(A).sparray().tocsc()
	# convert to Compressed Sparse Column format
	# To increase efficiency (??!)
	Asp_inv = scipy.sparse.linalg.inv(Asp)
	# And the Z matrix is now
	Z = S.dot(Asp_inv)
	# Now generate the Hessian and the right hand side for linear algebra
	Q = Z.T.dot(Gamma_noise_inv.dot(Z)) + Gamma_prior_inv
	Q.dot(x)
	### TIMING ENDS HERE
	t1 = time.time()
	matrix_free_time.append(t1-t0)

if __name__ == "__main__":
	matrix_free_time = []

	for i in xrange(100):
		main(matrix_free_time)
		if (i % 10 == 0):
			print i

	print np.mean(matrix_free_time)


# ############### MATRIX ACTION ###########################
# ############### for Qx, which is Qf######################
# def Qmatrix_action(K, x, Gprior, Gnoise, Spc, u):
# 	# Here x is a numpy vector and A is EigenMatrix
# 	# C is the middle matrix
# 	# Create Eigen Solver
# 	solver = EigenLUSolver("sparselu")
# 	solver.set_operator(K)
# 	# Store final product of Qx
# 	y = np.zeros(n)
# 	y += Gamma_prior_inv.dot(x)
# 	# Create function
# 	x2 = Vector(u.vector())
# 	x2[:] = x
# 	x1 = Vector()
# 	# Now solve
# 	solver.solve(x1, x2)
# 	# This be the middle matrix
# 	C = Spc.T.dot(Gnoise.dot(Spc))
# 	# Now solve the next problem with
# 	x2[:] = C.dot(x1.array())
# 	solver.solve_transpose(x1, x2)
# 	y += x1.array()
# 	return y

# y = Qmatrix_action(A, x, Gamma_prior_inv, Gamma_noise_inv, S, u)
# print y
# print np.linalg.norm(y - Q.dot(x))