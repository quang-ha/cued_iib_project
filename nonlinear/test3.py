# Solving Poisson Equation with optimisation
from dolfin import *

parameters["linear_algebra_backend"] = "PETSc"

# Create mesh
k = 16
mesh = IntervalMesh(64, 1.0, 5.0)

# Create function space
V = FunctionSpace(mesh, "Lagrange", 1)

# Create solution, trial and test functions
u, du, v = Function(V), TrialFunction(V), TestFunction(V)

# Variational formulation
f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)")
g = Constant(1.0)

L = f * u * dx + g * u * ds
gamma = 0.1
obj = gamma*u**2*dx + .5 * grad(u)**2 * dx - L
# obj = inner((1 + u**2)*grad(du), grad(v))*dx - f*v*dx

# Compute the jacobian
grad_obj = derivative(obj, u, v)
# Compute the Hessian
hess_obj = derivative(grad_obj, u, du)

# Box constrains
constraint_l = Expression("x_min", x_min=-1.2)
constraint_u = Expression("x_max", x_max=1.2)

u_min = interpolate(constraint_l, V)
u_max = interpolate(constraint_u, V)

# Define the minimisation problem by using OptimisationProblem class
class Problem(OptimisationProblem):

    def __init__(self):
        OptimisationProblem.__init__(self)

    # Objective function
    def f(self, x):
        u.vector()[:] = x
        return assemble(obj)

    # Gradient of the objective function
    def F(self, b, x):
        u.vector()[:] = x
        assemble(grad_obj, tensor=b)

    # Hessian of the objective function
    def J(self, A, x):
        u.vector()[:] = x
        assemble(hess_obj, tensor=A)

# Create the PETScTAOSolver
solver = PETScTAOSolver()

# Set some parameters
solver.parameters["method"] = "tron"
solver.parameters["monitor_convergence"] = True
solver.parameters["report"] = True
solver.parameters["maximum_iterations"] = 1000
#info(parameters, True)    # Uncomment this line to see the available parameters

# Parse (PETSc) parameters
parameters.parse()

# Solve the problem
set_log_level(DEBUG)
solver.solve(Problem(), u.vector(), u_min.vector(), u_max.vector())

# Save solution in VTK format
# ufile_pvd = File("velocity.pvd")
# ufile_pvd << u

# Plot solution
plot(u, interactive=True)