from dolfin import *
import numpy as np
from scipy.spatial.distance import euclidean
import scipy.sparse.linalg

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

# Extract the matrix
parameters['linear_algebra_backend'] = 'Eigen'

# Sub domain for Dirichlet boundary condition
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Create mesh and define function space
mesh = IntervalMesh(32, 1.0, 5.0)
V = FunctionSpace(mesh, "Lagrange", 3)

# Define boundary condition
g = Constant(1.0)
bc = DirichletBC(V, g, DirichletBoundary())	

# Define constants
A = 1.0
E0 = 1.0

# Define variational problem
u, du, v = Function(V), TrialFunction(V), TestFunction(V)
f = Expression("sin(x[0])")

# Let's try and write it in a strong form
# F = inner((1 + u**2)*grad(du), grad(v))*dx - f*v*dx
L = f * u * dx + g * u * ds
gamma = 0.1
F = gamma*u**2*dx + .5 * grad(u)**2 * dx - L

# Now compute the Jacobian and Hessian
u_p = Function(V) # The most recently computed solution
R = action(F, u_p)
DR = derivative(R, u_p) # Jacobian
DDR = derivative(DR, u_p) # Hessian
# print DDR
# exit(1)

# I want to see the action...
w = Constant(3.0)
b = action(DR, w)
B = assemble(b)
BSp = as_backend_type(B).array()
print BSp

# problem = NonlinearVariationalProblem(R, u_p, bc, DR)
# solver = NonlinearVariationalSolver(problem)
# solver.solve()

# # Plot the solution
# plot(u_p, interactive=True)