from ufl import * 
from dolfin import *
import numpy as np
import time
import matplotlib.pyplot as plt

T1 = []
T2 = []

for n in range(1,1000):
	# Create mesh and define function space
	mesh = UnitSquareMesh(n, n)
	V = FunctionSpace(mesh, "Lagrange", 1)

	# Define variational problem
	u = TrialFunction(V)
	v = TestFunction(V)
	a = u*v*dx

	# Try applying the action
	w = Constant(3.0)

	t0 = time.time()
	b = action(a, w)
	t1 = time.time()
	T1.append(t1-t0)

	# B1 = assemble(b).array()

	# Now try doing the matrix multiplication itself
	if (n<=200):
		A = assemble(a).array()
		x = 3.0*np.ones(A.shape[0])

		t0 = time.time()
		B2 = np.dot(A,x)
		t1 = time.time()
		T2.append(t1-t0)

		
		# x = w.vector()

		# print "L2 norm: ",np.linalg.norm(B1-B2)

# Now let's plot
plt.title("Comparison between Matrix-Free Method and Conventional Method")
plt.xlabel("Number of nodes")
plt.ylabel("Time taken")
plt.loglog(np.linspace(1.0, np.size(T1), np.size(T1)), T1, linewidth=2.0)
plt.loglog(np.linspace(1.0, np.size(T2), np.size(T2)), T2, linewidth=2.0)
plt.grid(True)
plt.tight_layout()
plt.show()




