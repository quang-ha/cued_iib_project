import cProfile, pstats, StringIO
import numpy as np
import re
from ufl import * 
from dolfin import *

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

T1 = []
T2 = []

n = 800

# Create mesh and define function space
mesh = UnitSquareMesh(n, n)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
a = u*v*dx + inner(grad(u), grad(v))*dx

# Try applying the action
w = Constant(3.0)
b = action(a, w)
# t0 = time.time()
pr = cProfile.Profile()
pr.enable()
#.... do sum ting... ##s
B = assemble(b)
#... stop do sum ting... # 
pr.disable()
# t1 = time.time()
# print "Matrix-Free method: ", t1-t0

# # Now try doing the matrix multiplication itself
# A = assemble(a)
# x = Vector()
# y = Vector()
# A.init_vector(x, 1)
# A.init_vector(y, 0)
# x[:] = 3.0
# A.mult(x, y)

s = StringIO. StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print s.getvalue()
# t0 = time.time()
# A.mult(x, y)
# t1 = time.time()
# print "Conventional method: ", t1-t0

# print "L2 norm: ",np.linalg.norm(B1 - B2)