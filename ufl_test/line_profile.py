from ufl import * 
from dolfin import *
import numpy as np
import time
import matplotlib.pyplot as plt

# @profile
def main(matrix_free_time):
	# Form compiler options
	parameters["form_compiler"]["optimize"]     = True
	parameters["form_compiler"]["cpp_optimize"] = True
	parameters["form_compiler"]["representation"] = "quadrature"

	T1 = []
	T2 = []

	n = 1000

	# Create mesh and define function space
	mesh = UnitSquareMesh(n, n)
	V = FunctionSpace(mesh, "Lagrange", 3)

	# Define variational problem
	u = TrialFunction(V)
	v = TestFunction(V)
	a = u*v*dx + inner(grad(u), grad(v))*dx

	# Try applying the action
	# t0 = time.time()
	# w = Constant(3.0)
	# b = action(a, w)
	# B = assemble(b)
	# t1 = time.time()
	# matrix_free_time.append(t1-t0)

	# Now try doing the matrix multiplication itself
	t0 = time.time()
	A = assemble(a)
	x = Vector()
	y = Vector()
	A.init_vector(x, 1)
	A.init_vector(y, 0)
	x[:] = 3.0
	A.mult(x, y)
	t1 = time.time()
	matrix_free_time.append(t1-t0)

	# print "L2 norm: ",np.linalg.norm(B1 - B2)

if __name__ == "__main__":
	matrix_free_time = []

	for i in xrange(100):
		main(matrix_free_time)
		if (i % 10 == 0):
			print i

	print np.mean(matrix_free_time)