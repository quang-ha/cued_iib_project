clear all

%addpath('.\library_vectorization')     %in windows
addpath('./library_vectorization/')     %in linux

levels=8; %maximal level of refinement, e.g. levels=11 for L-shape geometry creates FEM matrices with 12.5 million of rows 

create_2D_mesh; %mesh for testing

coeffs=sum(evaluate_average_point(elements,coordinates).^2,2); %elementwise coefficient depending on a square of distance to the origin

figure(1); subplot(1,2,1); show_constant_scalar(coeffs,elements,coordinates); view(2); axis off; title(strcat('elementwise constant coefficient on the mesh, level=',num2str(0)));

for level=0:levels    
    %uniform refinement
    if (level>0)
        [coordinates,elements,dirichlet]=refinement_uniform(coordinates,elements,dirichlet);
    end
    
    %save(strcat(strcat('meshTriangularLevel',num2str(level)),'.txt'),'coordinates','elements','-ASCII','-tabs','-double');
  
    coeffs=sum(evaluate_average_point(elements,coordinates).^2,2); %elementwise coefficient depending on a square of distance to the origin
    
    if level==3
        subplot(1,2,2); show_constant_scalar(coeffs,elements,coordinates); view(2); axis off; title(strcat('elementwise constant coefficient on the mesh, level=',num2str(level)));
    end
    
    %stiffness matrix assembly
    tic
    [K,areas]=stifness_matrixP1_2D(elements,coordinates, coeffs);
    time1(level+1)=toc; 
    
    %mass matrix assembly 
    tic
    M=mass_matrixP1_2D(elements,areas,coeffs);
    time2(level+1)=toc;
    
    %ones(1,size(coordinates,1))*M*ones(size(coordinates,1),1)   %testing
    %only
    
    rows(level+1)=size(K,1);
       
    fprintf('level=%d, ', level);
    fprintf('time spent on K=%f, ',time1(level+1));
    fprintf('time spent on M=%f, ',time2(level+1));
    fprintf('size of square matrices K,M =%d ',rows(level+1));
    fprintf('\n');
end




