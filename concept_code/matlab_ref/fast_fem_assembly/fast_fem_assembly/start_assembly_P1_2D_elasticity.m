%clear all

%addpath('.\library_vectorization')     %in windows
addpath('./library_vectorization/')     %in linux

levels=5; %maximum uniform refinement level

lambda=1; mu=1; %Lamme coeficients

create_2D_mesh; %mesh for testing

figure(1); subplot(1,2,1); show_mesh(elements,coordinates); title(strcat('coarsest mesh, level=',num2str(0)));

for level=0:levels    
    %uniform refinement
    if (level>0)
        [coordinates,elements,dirichlet]=refinement_uniform(coordinates,elements,dirichlet);
    end
    %[coordinates,elements3]=refinement_uniform(coordinates,elements3);
    
    %stiffness matrix assembly
    tic
    %[K,areas]=stifness_matrixP1_2D_elasticity(elements3,coordinates);
    [K areas]=stifness_matrixP1_2D_elasticity(elements,coordinates,lambda,mu);
    time1(level+1)=toc; 
    
    %mass matrix assembly
    tic
    M=mass_matrixP1_2D_elasticity(elements,areas);
    time2(level+1)=toc;
    
    rows(level+1)=size(K,1);
       
    fprintf('level=%d, ', level);
    fprintf('time spent on K=%f, ',time1(level+1));
    fprintf('time spent on M=%f, ',time2(level+1));
    fprintf('size of square matrices K,M =%d ',rows(level+1));
    fprintf('\n');
end

figure(1); subplot(1,2,2); show_mesh(elements,coordinates); title(strcat('finest mesh, level=',num2str(levels)));


