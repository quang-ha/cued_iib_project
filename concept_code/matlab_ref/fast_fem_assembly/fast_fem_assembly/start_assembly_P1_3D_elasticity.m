clear all

%addpath('.\library_vectorization')      %in windows
addpath('./library_vectorization/')      %in linux

levels=3; %maximum uniform refinement level
lambda=1; mu=1; %Lamme coeficients

load mesh3D.mat     %This replace the mesh generation for older versions of Matlab, eg. R2008b

%load coordinates.dat; load elements.dat; elements3=elements; clear elements;
%old code
%%Assembly
figure(1); show_mesh(elements3,coordinates); title('coarse mesh');

time1=zeros(levels,1); time2=time1;
for level=0:levels    
    %uniform refinement
    
    if (level>0)
        [coordinates,elements3]=refinement_uniform3D(coordinates,elements3);
    end
        
    %stiffness matrix assembly
    tic
    [K,areas]=stifness_matrixP1_3D_elasticity(elements3,coordinates,lambda,mu);
    time1(level+1)=toc; 
    rows(level+1)=size(K,1);
       
    fprintf('level=%d, ', level);
    fprintf('time spent on stifness matrix K=%f, ',time1(level+1));
    fprintf('size of square matricex K =%d, ',rows(level+1));
    fprintf('\n');
end


