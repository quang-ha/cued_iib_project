clear all

%addpath('.\library_vectorization')      %in windows
addpath('./library_vectorization/')      %in linux

levels=7; 

load hexmesh.mat   %meshes up to level 7, provided by TIEN DAT NGO (EPFL Lausanne), created in LS-PrePost

elements=hex{1}.elements;
coordinates=hex{1}.coordinates;

figure(1); show_mesh(elements,coordinates); title('coarsest mesh');

% time1=zeros(levels,1); time2=time1;
for level=0:levels   
     %uniform refinement
     if (level>0)
        elements=hex{level+1}.elements;
        coordinates=hex{level+1}.coordinates;
     end
     
%stiffness matrix assembly
tic
[K,areas]=stifness_matrixQ1_3D_elasticity(elements,coordinates,1,1);
time1(level+1)=toc; 

%mass matrix assembly
%     tic
%     M=mass_matrixP1_3D(elements3,areas);
%     time2(level+1)=toc;
%     
    rows(level+1)=size(K,1);
       
    fprintf('level=%d, ', level);
    fprintf('time spent on stifness matrix K=%6.1e, ',time1(level+1));
    %fprintf('time spent on mass matrix M=%%10.3e, ',time2(level+1));
    fprintf('size of square matrix K=%d, ',rows(level+1));
    fprintf('\n');
end

figure(2); show_mesh(elements,coordinates); title('finest mesh');

%creating hexahedral mesh on a unit cube - not working yet!
% NE_X=2;
% NE_Y=1;
% NE_Z=1;
% [X Y Z] = meshgrid(0:(1/NE_X):1,0:(1/NE_Y):1,0:(1/NE_Z):1);
% NN=(NE_X+1)*(NE_Y+1)*(NE_Z+1);
% coordinates=[reshape(X,NN,1,1) reshape(Y,NN,1,1) reshape(Z,NN,1,1)];
% element=[1 2 3+NE_X 2+NE_X [1 2 3+NE_X 2+NE_X]+(NE_X+1)*(NE_Y+1)];
% elements=element;
% for i=1:NE_X-1
%     elements=[elements; element+i];
% end


