%maximal level for my computer with 8Gb memory is 11 
%(it creates a triangular mesh with 25 milion elements)! 
levels=10;        


coordinates=[0  0; 1  0;  2  0; 2   1; 1    1; 0    1;  0   2;  1   2];
elements=[1 2 5; 5 6 1; 2 3 4; 2 4 5; 6 5 8; 6 8 7];
show_mesh(elements,coordinates);
fprintf('coarse mesh visualized, refining mesh, levels ');

for i=1:levels
[coordinates,elements]=refinement_uniform(coordinates,elements);   
fprintf('%d ', i);
end
fprintf(', finest mesh ready \n', i);
fprintf('number of elements=%d, ',size(elements,1));
fprintf('number of nodes=%d \n',size(coordinates,1));
fprintf('\n');

if 0
    fprintf('vectorized version 2, ');
    tic
    a=coordinates(elements(:,1),1);
    b=coordinates(elements(:,2),1);
    c=coordinates(elements(:,3),1);
    d=coordinates(elements(:,1),2);
    e=coordinates(elements(:,2),2);
    f=coordinates(elements(:,3),2);
    ca=c-a;
    cb=c-b;
    fd=f-d;
    fe=f-e;
    areas2=abs(ca.*fe - cb.*fd)/2; 
    time_vectorized=toc;
    fprintf('time spent (in seconds)=%f \n', time_vectorized);
else
    fprintf('vectorized version, ');
    tic
    a=coordinates(elements(:,1),1);
    b=coordinates(elements(:,2),1);
    c=coordinates(elements(:,3),1);
    d=coordinates(elements(:,1),2);
    e=coordinates(elements(:,2),2);
    f=coordinates(elements(:,3),2);
    areas=abs(b.*f - c.*e - a.*f + c.*d + a.*e - b.*d)/2; 
    time_vectorized=toc;
    fprintf('time spent (in seconds)=%f \n', time_vectorized);
end

fprintf('serial version, ');
tic
areas=zeros(size(elements,1),1);
for i=1:size(elements,1)
    a=coordinates(elements(i,1),1);
    b=coordinates(elements(i,2),1);
    c=coordinates(elements(i,3),1);
    d=coordinates(elements(i,1),2);
    e=coordinates(elements(i,2),2);
    f=coordinates(elements(i,3),2);
    areas(i)=abs(a*e + b*f + c*d - a*f - b*d - c*e)/2;
end    
time_serial=toc;
fprintf('time spent (in seconds)=%f \n', time_serial);
fprintf('\n');

fprintf('vectorized code is %f times faster than serial code! \n', time_serial/time_vectorized);





