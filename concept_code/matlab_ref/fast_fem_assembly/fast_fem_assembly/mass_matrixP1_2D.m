function M=mass_matrixP1_2D(elements,areas,coeffs)
%coefficients represent a vector of elementwise constant coefficients
%it is a collumn vector with size(elements,1) entries
%if coeffs is not provided then coeffs=1 in all elements

Xscalar=kron(ones(1,3),elements); Yscalar=kron(elements,ones(1,3)); 

if (nargin<3)
    Zmassmatrix=kron(areas,reshape((ones(3)+eye(3))/12,1,9)); 
else
    Zmassmatrix=kron(areas.*coeffs,reshape((ones(3)+eye(3))/12,1,9)); 
end

M=sparse(Xscalar,Yscalar,Zmassmatrix);