{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MOTIVATION # "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ultrasound has been extensively used in diagnosing breast cancers, and in studying the mechanical properties of the breast tissues. In this experiment, a bag of canola oil is used as a median between the ultrasound probe and the breasts. The displacement of the membrane is captured at each increment of pressure on the oil bag. From the results of these B-mode images captured by the scan, one wish to determine the mechanical properties of the breast tissues, and also by identifying the *stiff* location, where the potential origin of tumour could be. Diagrams are provided below to help understanding the experimental set-up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"image123\">\n",
    "    <img src=\"pic/scan_concept.png\" width=\"45%\" style=\"float:left\">\n",
    "    <img src=\"pic/scan_example.jpg\" width=\"50%\" style=\"float:right\">\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "On the left side is the conceptual diagram of the experiment. A bag of oil is placed between the ultrasound probe and the patient's breast. Applying a pressure at the indicated level causes the oil bag to exert a force onto the breast. At each increment in pressure, the bag is pressed more against the breast tissues, causing deformations in both the tissues and the bag's membrane. On the right side is a B-mode image sliced in lateral direction of one volume scan. Breast tissues only shows up in areas where there is contact between the oil bag and the membrane. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PROBLEM DEFINITION AND FORMULATION # "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to provide better illustration of the problem, a force diagram between the oil bag and the breast is presented below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"pic/force_diagram.png\" width=\"80%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the green region, there is no contact force $f(x,y)$, hence the only force that determines the configuration of the membrane is the tension, labelled $T$ on the diagram.\n",
    "\n",
    "A membrane in equilibrium occupies a position $x_3 = h(x_1,x_2)$, for $(x_1,x_2) \\in \\Omega \\subset R^2$. Here $h$ represents the membrane deformation.\n",
    "The membrane is acted upon by a known pressure distribution $p_{o}(x_1,x_2)$\n",
    "on the top surface ($x_3 = h^+$), and an unknown pressure distribution \n",
    "on the lower  surface ($x_3 = h^-$), $f(x_1,x_2)$.   It is known, however, that \n",
    "the support of $ f(x_1,x_2) = D \\subset \\Omega$:\n",
    "\n",
    "$$\n",
    "f(x_1,x_2) = 0 \\quad \\mbox{for } (x_1,x_2) \\notin D\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## THE CONTACT REGION ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The assumptions above lead to the *force equilibrium* equation on the membrane: \n",
    "$$\n",
    "T\\nabla^2 h = p_0 + \\rho g h - f, \\qquad x \\in \\Omega \n",
    "$$\n",
    "\n",
    "Now assuming that self-weight of the oil bag is negligible, we can rewrite the equation as followed:\n",
    "$$\n",
    "T\\nabla^2 h = p_0 - f, \\qquad x \\in \\Omega \n",
    "$$\n",
    "\n",
    "Certain assumptions are made on the membrane. \n",
    "\n",
    "- Firstly, it is assumed that the membrane tension $T$ is *uniform* and *isotropic*. If $T$ were not uniform (i.e. constant in space), the left hand side would be modified to be $\\nabla \\cdot (\\mathbf{T}\\nabla h)$. At the same time, if $T$ were not isotropic, so that the membrane had different tensions in different directions, then the left hand side would be again modified to be $\\nabla \\cdot (\\mathbf{T}\\nabla h)$, but now $\\mathbf{T}$ is a tensor. \n",
    "\n",
    "- Secondly, there is \\textit{no friction} acting on the membrane. This means that all tractions act normal to the surface.\n",
    "\n",
    "From those assumptions, the tension is calculated from the *non-contact* area, and the result of this constant tension is then fed back to the formulation above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## THE NON-CONTACT REGION ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this area, as mentioned above, there is no force exerted by the breast onto the membrane, giving $f = 0.0$ in this region. Hence, the only external force acting on the membrane is the oil pressure. The force balance can be written as follows:\n",
    "\n",
    "$$\n",
    "T\\nabla^2 h = p_0, \\qquad x \\in \\Omega \n",
    "$$\n",
    "\n",
    "Hence, this region is used to solve for the membrane tension. The approaches applied for the corresponding regions are listed below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FORWARD AND INVERSE PROBLEM #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The forward problem is stated as followed: given any force $f$ and pressure distribution $p_0$, the membrane configuration can easily be obtained using Finite Element Method. In other words, there exists an operator that can construst the membrance configuration $h$ from the forces applied on the membrane, and let's called that $G(f, p_0)$ such that:\n",
    "\n",
    "$$\n",
    "h = G(f, p_0), \\qquad x \\in \\Omega\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, in practical, the forces acted on the membranes are unknown, and only the membrane configuration can be obtained via ultrasound imaging of the breasts. After capturing the ultrasound scan image of the breast, one wants to determine the force applied on the membrane itself. Combining this applied stress with the strains measured by *elastography imaging* allows the quantification of the mechanical properties of breast tissues. This could be used for early stage of breast cancer diagnosis. To sum up, we want to have an operator that will map our membrane to the corresponding distributing stresses on the membrane, which is: \n",
    "\n",
    "$$\n",
    "f = F(h), \\qquad x \\in \\Omega\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In terms of matrix operation, we can have: \n",
    "\n",
    "$$\n",
    "\\mathbf{h} = \\mathbf{G_1}(\\mathbf{f} + \\mathbf{p_0})\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\mathbf{f} = \\mathbf{F}\\mathbf{h} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To create more difficulties, our observable data $h$ is contaminated with random noise data, making it into $\\tilde{h} = h + n $ where $n$ represents the level of noise in the data input. \n",
    "\n",
    "Before dwelling further in the current inverse problem on the membrane, let's look at the nature and properties of the inverse problems, along with how a Bayesian framework can be applied to tackle them. These are discussed in this [Inverse Polynomial Fitting](Polynomial.ipynb) notebook.\n",
    "\n",
    "[Overview](Overview.ipynb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
