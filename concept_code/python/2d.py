from func import * 
from matplotlib import cm
from matplotlib.pyplot import figure, show
from mpl_toolkits.mplot3d import Axes3D

################FORWARD PROBLEM ######################
k_1 = stiffness_matrix_gen(L, n_cells)
m_1 = mass_matrix_gen(L, n_cells)
k2d_1 = np.kron(m_1, k_1) + np.kron(k_1, m_1)
print k2d_1

n_nodes = n_cells + 1 # 1D

# Calculate the total number of nodes and cells
total_nodes = n_nodes**2
total_cells = n_cells**2

x = np.linspace(0.0, 1.0, n_nodes)
y = np.linspace(0.0, 1.0, n_nodes)

K_global = np.zeros((total_nodes, total_nodes))
print K_global.shape	

for i in range(n_cells):
	for j in range(n_cells):
		# Get the index on global DOF
		el = []
		el.append(j+n_nodes*i)
		el.append(j+n_nodes*i+1)
		el.append(j+n_nodes*(i+1)+1)
		el.append(j+n_nodes*(i+1))
		# Now iterate through local DOF
		for ii in range(4):
			for jj in range(4):
				K_global[el[ii], el[jj]] += k2d_1[ii, jj]

# print K_global

# Define the applied force vector
def f_1(x, y):
	return 5.0
	# return 100*np.sin(4*np.pi*x)*np.sin(4*np.pi*y)

## Now we need to numerical integrate the function 2D
# Two-point Gauss quadrature points on the interval (-1, 1)
quadrature = [[-1.0/np.sqrt(3.0), 1.0/np.sqrt(3.0)], [-1.0/np.sqrt(3.0), 1.0/np.sqrt(3.0)]]

# Assemble RHS using two-point Gauss quadrature
b = np.zeros(total_nodes)    
for i in range(n_cells):
	for j in range(n_cells):
	    # Get cell midpoint
		midpoint = ((x[i]+x[i+1])/2.0, (y[j]+y[j+1])/2.0)
	    # Loop over quadrature points
		bl = (midpoint[0]-1.0/np.sqrt(3.0), midpoint[1]-1.0/np.sqrt(3.0))
		br = (midpoint[0]+1.0/np.sqrt(3.0), midpoint[1]-1.0/np.sqrt(3.0))
		tr = (midpoint[0]+1.0/np.sqrt(3.0), midpoint[1]+1.0/np.sqrt(3.0))
		tl = (midpoint[0]-1.0/np.sqrt(3.0), midpoint[1]+1.0/np.sqrt(3.0))

		# Indexing
		bli = j+n_nodes*i
		bri = bli+1
		tli = bli+n_nodes
		tri = tli+1

		# weight, equal at these Gauss points
		w = 1.0*(h/4.0)

		## Now let's erm... calculate at each node
		# Bottom Left
		Nbl = 0.25*(1-bl[0])*(1-bl[1])
		b[bli] += w*Nbl*f_1(bl[0], bl[1])

		# Bottom RIght
		Nbr = 0.25*(1+br[0])*(1-br[1])
		b[bri] += w*Nbr*f_1(br[0], br[1])

		# Top Right
		Ntr = 0.25*(1+tr[0])*(1+tr[1])
		b[tri] += w*Ntr*f_1(tr[0], tr[1])

		# Top Left
		Ntl = 0.25*(1-tl[0])*(1+tl[1])
		b[tli] += w*Ntl*f_1(tl[0], tl[1])

		# print i,j,bli, bri, tri, tli
		# print b

print b.shape

## Now we need to apply BC

for i in range(n_nodes):
	# Apply to bottom
	K_global[i, :], K_global[:, i], K_global[i, i] = 0.0, 0.0, 1.0
	b[i] = 0.0

	# Apply to left
	il = i*n_nodes
	K_global[il, :], K_global[:, il], K_global[il, il] = 0.0, 0.0, 1.0
	b[il] = 0.0

	# Apply to right
	ir = i*n_nodes + (n_nodes-1)
	K_global[ir, :], K_global[:, ir], K_global[ir, ir] = 0.0, 0.0, 1.0
	b[ir] = 0.0

	# Apply to top
	it = i + (n_nodes-1)*n_nodes
	K_global[it, :], K_global[:, it], K_global[it, it] = 0.0, 0.0, 1.0
	b[it] = 0.0

A = np.linalg.inv(K_global)

# Solve for displacement
h = cg(K_global, b)[0]

######################################################################
# Define beta_prior and beta_noise
beta_prior = 0.01
beta_noise = 1.0

# Now generate those covariance matrices
Gamma_prior = 1.0/(beta_prior)*np.identity(h.size)
Gamma_noise = 1.0/(beta_noise)*np.identity(h.size)

# Generate fake data
noise = np.random.normal(0, (1.0/beta_noise), h.size) 
h_obs = h + 0.01*(noise - np.mean(noise)) # noisy obs
h_prior = 4.0*np.ones(b.size) # prior knowledge

# Solve the normal stuff
M_1 = (A.T.dot(np.linalg.inv(Gamma_noise).dot(A))) + np.linalg.inv(Gamma_prior)
c_1 = A.T.dot(np.linalg.inv(Gamma_noise).dot(h_obs)) + np.linalg.inv(Gamma_prior).dot(h_prior)
	
f_hat = cg(M_1, c_1)[0]

# And now let's plot the surface
X, Y = np.meshgrid(x, y)
Z = np.reshape(f_hat, (n_nodes, n_nodes))

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=False)

plt.show()
exit(1)



# Compute misfit matrix H_misfit
# H_misfit = np.sqrt(Gamma_prior).dot(A.T.dot(np.linalg.inv(Gamma_noise).dot(A.dot(np.sqrt(Gamma_prior)))))