import numpy as np

import matplotlib.pyplot as plt
from scipy.sparse.linalg import cg

# This code is adapted from Garth N. Wells notebook
# which can be found here: http://goo.gl/VZ8W46

######### DEF ########################
def mesh_gen(L, n_cells):
    # Crarte mesh and compute cell size
    n_nodes = n_cells + 1
    mesh = np.linspace(0.0, L, n_nodes)
    return mesh
    
def stiffness_matrix_gen(L, n_cells):
    n_nodes = n_cells + 1
    # Compute locall stiffness matrix
    k_e = -(T/h)*np.array([[1, -1], [-1, 1]])
    
    # # Assemble global stiffnes matrix
    # K = np.zeros((n_nodes, n_nodes))
    # for element in range(n_cells):
    #     K[element:element + 2, element:element + 2] += k_e
            
    return k_e

def mass_matrix_gen(L, n_cells):
    n_nodes = n_cells + 1
    # Compute locall mass matrix
    m_e = -(T/h)*np.array([[1.0/3.0, 1.0/6.0], [1.0/6.0, 1.0/3.0]])
    
    # # Assemble global mass matrix
    # M = np.zeros((n_nodes, n_nodes))
    # for element in range(n_cells):
    #     M[element:element + 2, element:element + 2] += m_e
            
    return m_e

def rhs_vector_gen(f, n_cells):
    n_nodes = n_cells + 1
    # Two-point Gauss quadrature points on the interval (-1, 1)
    x_quadrature = ((-1.0/np.sqrt(3.0), 1.0), (1.0/np.sqrt(3.0), 1.0))    

    # Assemble RHS using two-point Gauss quadrature
    b = np.zeros(n_nodes)    
    for element in range(n_cells):

        # Get cell midpoint
        x_mid = (mesh[element + 1] + mesh[element])/2.0    

        # Loop over quadrature points
        for zeta, weight in x_quadrature:        
            # Compute coordinate of point 
            x = x_mid + zeta*h/2.0

            # Evaluate loading term
            f_load = f(x) - p0

            # Quadrature weighta
            w = weight*(h/2.0)
            
            # Compute RHS contributions
            N = 0.5 - zeta/2.0 
            b[element] += w*N*f_load 

            N = 0.5 + zeta/2.0 
            b[element + 1] += w*N*f_load

    return b

def f_1(x):
    return np.ones(x.size) 

def h_1(x):
    return ((1.0-p0)/(2*T))*x*(x - L)

def f_sin(x):
    return np.sin((np.pi*x)/L)

def h_sin(x):
    return -(L**2/np.pi**2)*np.sin((np.pi*x)/L)

def gen_filt_mat(x, m):
    j = 0
    R = np.zeros((x.size, m.size))
    for i in range(x.size):
        for j in range(m.size):
            if m[j] > x[i]:
                break
        if (m[j] > x[i] and x[i] > m[j-1]):
            R[i, j-1] = (m[j] - x[i])/h
            R[i, j] = (x[i] - m[j-1])/h
        elif m[j] == x[i]:
            R[i, j] = 1.0
        elif m[j-1] == x[i]:
            R[i, j-1] = 1.0
    return R

def cg_2(M, c):
    "Conjugate gradient solver"
    # Initial guess (zero vector)
    x0 = np.zeros(M.shape[1])

    # Convergence tolerance to exit solver
    tolerance = 1.0e-9

    # Create starting vectors
    r0 = c - M.dot(x0)
    r0_norm = np.linalg.norm(r0)
    p0 = r0.copy()
    
    # Start iteration 0 
    # 0 -> k; 1 -> k+1, 
    alpha = r0.dot(r0)/(p0.dot(M.dot(p0)))
    x1 = x0 + alpha*p0
    r1 = r0 - alpha*M.dot(p0)
    a = (1.0/alpha)*(r0 - r1)
    beta = r1.dot(r1)/r0.dot(r0)
    p1 = r1 + beta*p0
    
    # Now let's throw these vectors into our matrices
    # First iteration, create copies
    V = r1.copy()
    A = a.copy()
    P = p1.copy()
    
    # Update for next step
#     p0, r0, x0 = p1, r1, x1

    
    # Break condition
    r_norm = np.linalg.norm(r1) 
    if r_norm > tolerance:
        # Start from iteration 1
        for k in range(1, 200):
            alpha = r0.dot(r0)/(p0.dot(a))
            x1 = x0 + alpha*p0
            # Update residual
            rhat1 = r0 - alpha*a
            # And then reorthogonise
            r1 = rhat1 - sum_vec(rhat1, V, V)

            # Break condition
            r_norm = np.linalg.norm(r1) 
            if r_norm < tolerance:
                break

            beta = r1.dot(r1)/r0.dot(r0)
            # Update search direction
            phat1 = r1 + beta*p0    
            ahat = (1.0/alpha)*(r1 - r0)
            # And then apply re-ortho
            a = ahat - sum_vec(ahat, P, A) 
            p1 = phat1 - sum_vec(phat1, A, P) 
            
            # Let's store the solution in some form of matrices
            V = np.vstack((V, r1))
            P = np.vstack((P, p1))
            A = np.vstack((A, a))
            
            # Update for next step
            p0, r0, x0 = p1, r1, x1
            # print k, "\n", p0, "\n", r0, "\n", r_norm
    
    print "Num iter: ", k
    return V, A, P, x1

def sum_vec(vec, Mat1, Mat2):
    results = np.zeros(vec.size)
    if Mat1.ndim == 1:
        results += ((1.0*(vec.dot(Mat1)))/(1.0*(Mat1.dot(Mat2))))*Mat2
    else:
        k = Mat1.shape[0]
        for i in range(k):
            results += ((1.0*(vec.dot(Mat1[i])))/(1.0*(Mat1[i].dot(Mat2[i]))))*Mat2[i]
    
    return results

########################################################
# Parameter for the problem
T = 1.0
p0 = 0.0

# Length and number of nodes
L = 1.0
n_cells = 10
h = L / n_cells # mesh size
mesh = mesh_gen(L, n_cells)
