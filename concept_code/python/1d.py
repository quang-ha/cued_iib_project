from func import * 

################FORWARD PROBLEM ######################
K_1 = stiffness_matrix_gen(L, n_cells)

# Apply boundary condition
K_1[0, :], K_1[:, 0], K_1[0, 0] = 0.0, 0.0, 1.0
K_1[n_cells, :], K_1[:, n_cells], K_1[n_cells, n_cells] = 0.0, 0.0, 1.0

# b_1 = rhs_vector_gen(f_1, n_cells)
b_sin = rhs_vector_gen(f_sin, n_cells)

# Apply boundary condition
# b_1[0] = 0.0
# b_1[n_cells] = 0.0
b_sin[0] = 0.0
b_sin[n_cells] = 0.0


# Exact solution
x_1_exact = np.linspace(0, L, 100)
# h_1_exact = h_1(x_1_exact)
h_sin_exact = h_sin(x_1_exact)

# FE solution - solve using CG
# h_1_fe = cg(K_1, b_1)[0]
h_sin_fe = cg(K_1, b_sin)[0]

# Get a matrix to only extract data at x = 1.5, 2.5 and 3.5
m_obs = 10 # number of obersations
n_pars = h_sin_fe.size # Number of observation

# How about writing a function to define that, from x_obs
x_obs = np.linspace(mesh[0], mesh[-1], m_obs)
filt = gen_filt_mat(x_obs, mesh)

########################################################


##################### INVERSE PROBLEM ##################

# Now let's plwrot these points
h_obs = filt.dot(h_sin_fe) 
A = filt.dot(np.linalg.inv(K_1))

# Define beta_prior and beta_noise
beta_prior = 10.0
beta_noise = 10.0

# Now generate those covariance matrices
Gamma_prior = (h**3)/(beta_prior)*np.identity(n_pars)
Gamma_noise = 1.0/(beta_noise)*np.identity(m_obs)

# Let's ignore the grad term for now (T.T)
# b_prior = 1.2*np.ones(b_sin.size)
b_prior = b_sin.copy()

# Generate fake data
noise = np.random.normal(0, (1.0/beta_noise), h_obs.size) 
h_obs_n = h_obs + (noise - np.mean(noise))

# Solve the normal stuff
M_1 = (A.T.dot(np.linalg.inv(Gamma_noise).dot(A))) + np.linalg.inv(Gamma_prior)
c_1 = A.T.dot(np.linalg.inv(Gamma_noise).dot(h_obs_n)) + np.linalg.inv(Gamma_prior).dot(b_prior)

# Now let's use CG to solve M_1 * b_hat = c_1
b_hat = cg(M_1, c_1)[0]

# Let's try and see the coveriance
Gamma_post = np.linalg.inv(M_1)

# Compute misfit matrix H_misfit
H_misfit = np.sqrt(Gamma_prior).dot(A.T.dot(np.linalg.inv(Gamma_noise).dot(A.dot(np.sqrt(Gamma_prior)))))

# And now let's find the eigenvalues and eigenvectors
EigVa, EigVe = np.linalg.eigh(H_misfit)

# Now truncate some eigenvalues out
thres = 0.5 * EigVa[-1]

i = EigVa.size - 1

while (EigVa[i] > thres):
    i -= 1
    
Vr = EigVe[:, i:]
D = EigVa[i:]

# Now make Dr a square matrix 
Dr = np.diag(D/(D+1.0))

# And let's look at the approximation
Gamma_post_approx = np.sqrt(Gamma_prior).dot((np.identity(n_pars) - Vr.dot(Dr.dot(Vr.T))).dot(np.sqrt(Gamma_prior)))

## How about CG Approximation
V, A, P, q = cg_2(M_1, c_1)

if P.ndim == 1:
    D = np.outer((1.0/P), (1.0/A))
    IH_approx = np.outer(P, (D.dot(P.T))) + np.identity(P.size) - np.outer(V, Gamma_prior.dot(V))
else:
    D = P.T.dot(A)
    invD = np.linalg.inv(D)
    IH_approx = P.T.dot(invD.dot(P.T)) + np.identity(P.shape[1]) - V.T.dot(Gamma_prior.dot(V.T))
    

U_approx, s_approx, V_approx = np.linalg.svd(IH_approx, full_matrices=False)

########################################################

# # Let's plot and compare...
# plt.figure(figsize=(12,10))
# plt.title("Values of b")
# plt.plot(b_sin,'-o', label='exact')
# plt.plot(b_hat,'-o', label='inverse')
# plt.legend()
# plt.show()
# exit(1)

# Plot the graph
fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(40, 20))

ax1.set_title('Exact')
im = ax1.imshow(Gamma_post, cmap='jet')

ax2.set_title('Approximation')
im = ax2.imshow(Gamma_post_approx, cmap='jet')

ax3.set_title('CG Approximation')
im = ax3.imshow(IH_approx, cmap='jet')

fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.36, 0.025, 0.3])
fig.colorbar(im, cax=cbar_ax)

plt.show()

