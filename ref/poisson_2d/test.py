import numpy as np
import orthopoly as op
from pseudospectral import diffmat
from scipy.linalg import solve

# Check the mass matrix on -1, 1 domain
n = 2
alpha,beta = op.rec_jacobi(n,0,0)  # Legendre recursion coefficients
# x,w = op.lobatto(alpha,beta,-1,1)  # LGL quadrature
x,w = op.gauss(alpha, beta)
print x
print w
D = diffmat(x)                     # Pseudospectral differentiation matrix
M = np.diag(w)                     # Approximate 1D mass matrix
K = np.dot(D.T,np.dot(M,D))        # 1D stiffness matrix

A = np.kron(K,M) + np.kron(M,K)    # Galerkin approximation of -Delta

# print M
print K
# print A