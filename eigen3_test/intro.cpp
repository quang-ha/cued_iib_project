#include <iostream>
#include <Eigen/Dense>  
using namespace std;
int main()
{
  int n = 10;
  // Initialise Ax=b problem
  Eigen::MatrixXd A;
  A = Eigen::MatrixXd::Random(10,10);
  // cout << "This matrix is: \n" << A << endl;
  Eigen::VectorXd b = Eigen::VectorXd::Random(n)*10;
  // VectorXd c = VectorXd::Random(n)*10;
  // cout << "This vector is: \n" << b << endl;

 //  //--**** ONE-SHOT SOLVER ***----//
 //  // Normal equation
 //  cout << "The solution using normal equations is:\n"
 //     << (A.transpose() * A).ldlt().solve(A.transpose() * b) << endl;

 //  // Jacobi
 //  cout << "The least-squares Jacobi solution is:\n"
	// << A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b) << endl;

 //  // Column Pivoting QR decomposition
 //  cout << "The solution using the QR decomposition is:\n"
 //     << A.colPivHouseholderQr().solve(b) << endl;

  // Check vector size output
  cout << A.Eigen::operator*(b) << endl;
  // cout << "Vector b: " << b << endl;
  // cout << "Vector c: " << c << endl;
  // cout << "Dot product: " << sqrt(b.dot(b)) << endl;
}
