#include <iostream>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <vector>
#include <Eigen/Core>
#include <chrono>
#include <ctime>

typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> T;
void buildProblem(std::vector<T>& coefficients, Eigen::VectorXd& b, int n);
void insertCoefficient(int id, int i, int j, double w, std::vector<T>& coeffs,
                       Eigen::VectorXd& b, const Eigen::VectorXd& boundary);
int main()
{ 
  // For chrono to work, this file needs to be 
  // compiled with C++ 11
  // g++ -std=c++11
  std::chrono::time_point<std::chrono::system_clock> start, end;

  Eigen::initParallel();
  // int num_core = Eigen::nbThreads();
  // std::cout<<"Number of cores: "<< num_core << std::endl;   

  int n = 200;  // size of vectors
  int m = n*n;  // number of unknows 
  // Assembly:
  std::vector<T> coefficients;            // list of non-zeros coefficients
  Eigen::VectorXd b(m);                   // the right hand side-vector resulting 
  buildProblem(coefficients, b, n);
  SpMat A(m,m);
  A.setFromTriplets(coefficients.begin(), coefficients.end());
  // Solving:
  start = std::chrono::system_clock::now();
  Eigen::ConjugateGradient<Eigen::SparseMatrix<double> > cg;
  cg.compute(A);
  Eigen::VectorXd x = cg.solve(b);
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end-start;

  // Print out results
  // std::cout << "This matrix is: \n" << A << std::endl;
  // std::cout << "The b vector is: \n" << b << std::endl;
  // std::cout << "The x vector is: \n" << x << std::endl;
  std::cout << "Number of iterations: " << cg.iterations() << std::endl;
  std::cout << "Tolerance: " <<  cg.tolerance() << std::endl;
  std::cout << "Solving time: " << elapsed_seconds.count() << "s\n";
  return 0;
}
//----------------****--------------//
//--------BUILD PROBLEM ------------//
//----------------****--------------//
void buildProblem(std::vector<T>& coefficients, Eigen::VectorXd& b, int n)
{
  b.setZero();
  Eigen::ArrayXd boundary = Eigen::ArrayXd::LinSpaced(n, 0,M_PI).sin().pow(2);
  for(int j=0; j<n; ++j)
  {
    for(int i=0; i<n; ++i)
    {
      int id = i+j*n;
      insertCoefficient(id, i-1,j, -1, coefficients, b, boundary);
      insertCoefficient(id, i+1,j, -1, coefficients, b, boundary);
      insertCoefficient(id, i,j-1, -1, coefficients, b, boundary);
      insertCoefficient(id, i,j+1, -1, coefficients, b, boundary);
      insertCoefficient(id, i,j,    4, coefficients, b, boundary);
    }
  }
}
//----------------****--------------//
//--------INSERT COEFFICIENT--------//
//----------------****--------------//
void insertCoefficient(int id, int i, int j, double w, std::vector<T>& coeffs,
                       Eigen::VectorXd& b, const Eigen::VectorXd& boundary)
{
  int n = int(boundary.size());
  int id1 = i+j*n;
        if(i==-1 || i==n) b(id) -= w * boundary(j); // constrained coefficient
  else  if(j==-1 || j==n) b(id) -= w * boundary(i); // constrained coefficient
  else  coeffs.push_back(T(id,id1,w));              // unknown coefficient
}

