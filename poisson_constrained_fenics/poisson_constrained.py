from dolfin import *
import numpy as np
from scipy.spatial.distance import euclidean
import scipy.sparse.linalg

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)

########### SOLVER STARTS HERE ##########################
# Define Dirichlet boundary
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Define some constant
beta = Constant(100.00)

# Create function space
F = FunctionSpace(mesh, "Lagrange", 1) # Function space for f
U = FunctionSpace(mesh, "Lagrange", 1) # Function space for u
LDA = FunctionSpace(mesh, "Lagrange", 1) # Function space for lda
W = MixedFunctionSpace([F, U, LDA])

# Create the hat function
UHAT = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)")
uhat = interpolate(UHAT, U)

# Apply BC, if needed, here
# zero = Constant(0.0)
# bc0 = DirichletBC(W.sub(0), zero, DirichletBoundary())
bc = [] 

## Define trial function and test function
(f, u, lda) = TrialFunction(W)
(df, du, dlda) = TestFunction(W)

## System of equation that needs to be solved
# First one
F1 = 2*beta*inner(f, df)*dx - inner(lda, df)*dx
a1 = lhs(F1)
L1 = rhs(F1)

# Second
F2 = inner(grad(u), grad(dlda))*dx - inner(dlda, f)*dx
a2 = lhs(F2)
L2 = rhs(F2)

# Last equation, with uhat
F3 = inner(du,(u - uhat))*dx + inner(grad(du), grad(lda))*dx
a3 = lhs(F3)
L3 = rhs(F3)
## Solve the system and split the answer
a = a1+a2+a3
L = L1+L2+L3
w = Function(W)
solve(a == L, w, bc)

# # Plotting the solution
f = w.split(deepcopy=True)[0]
u = w.split(deepcopy=True)[1]
lda = w.split(deepcopy=True)[2]

# plot(f, title='Solution for f')
# plot(u, title='Solution for u')
# plot(uhat, title='Expected u')
# plot(lda, title='Solution for lambda')
# interactive()

#### Let's try and capture the sparse matrix into Scipy
parameters['linear_algebra_backend'] = 'Eigen'

# Get the sparse matrix from a
A = assemble(a)
ASp = as_backend_type(A).sparray()

# Now the sparse matrix from b
B = assemble(L)
BSp = as_backend_type(B).array()
print len(BSp)
exit(1)
# Solve the matrix
[X, tol] = scipy.sparse.linalg.cg(ASp, BSp, tol=1e-8) 

# Split each element out?
F1 = f.vector().array()
U1 = u.vector().array()
LD1 = lda.vector().array()

# Because the way the assemble works, it stores F, U and LDA 
# in a sequence
F2 = X[::3]
U2 = X[1::3]
LD2 = X[2::3]

# print np.linalg.norm(F1-F2)
# print np.linalg.norm(U1-U2)
# print np.linalg.norm(LD1-LD2)
