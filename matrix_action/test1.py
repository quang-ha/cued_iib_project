"This test the action for sparse array"

import numpy, scipy.sparse
import timeit

n = 100000
x = (numpy.random.rand(n) * 2).astype(int).astype(float) # 50% sparse vector
x_csr = scipy.sparse.csr_matrix(x)
x_dok = scipy.sparse.dok_matrix(x.reshape(x_csr.shape))

print repr(x_csr)

# timeit numpy.dot(x, x)
# timeit.timeit("numpy.dot(x,x)", number=10000)